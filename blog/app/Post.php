<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Tag;


class Post extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'category_id',
        'post_title',
        'post_title_clean',
        'post_thumbnail',
        'post_content',
        'post_status',
        'post_comment_status',
        'post_timer',
        'post_view',
        'post_like',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    /**
     * Get the comments for the blog post.
     */
    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    /**
     * The tags that belong to the post.
     */
    public function tags($id)
    {
        $tags = Tag::orderBy('tag_title_clean')
            ->where('post_id', $id)
            ->select('tag_title', 'tag_title_clean')
            ->get();

        return $tags;
    }

    public function likes()
    {
        return $this->hasMany('App\PostLike');
    }

    /*public function postsOfTag($tagTitleClean, $perPage, $length)
    {
        $tags = Tag::where('tag_title_clean', $tagTitleClean)
            ->select('post_id')
            ->get();

        if (!empty($tags))
        {
            $posts = array();

            foreach ($tags as $tag)
            {
                array_push($posts, $tag->post_id);
            }

            $items = $this::orderBy('post_title_clean')
                ->join('categories', 'posts.category_id', '=', 'categories.id')
                ->join('users', 'posts.user_id', '=', 'users.id')
                ->whereIn('posts.id', $posts)
                ->select('posts.*', 'categories.category_title', 'categories.category_title_clean' ,'users.name')
                ->paginate($perPage);

            $this->strimContent($items, $length);

            return $items;
        }
        else
        {
            abort(404);
        }
    }*/

    /**
     * Get the category that owns the post.
     */
    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function getList($category, $tag, $username, $sort, $perPage, $length)
    {
        $now = Carbon::now()->timestamp;
        if (!empty($username))
        {
            $user = User::where('name', $username)->first();
        }

        if (!empty($category))
        {
            $category = Category::where('category_title_clean', $category)->first();
        }

        $postsAr = array();
        if (!empty($tag))
        {
            $tags = Tag::where('tag_title_clean', $tag)
                ->select('post_id')
                ->get();

            if (!empty($tags))
            {
                //$posts = array();

                foreach ($tags as $tag)
                {
                    array_push($postsAr, $tag->post_id);
                }

//                $items = $this::orderBy('post_title_clean')
//                    ->join('categories', 'posts.category_id', '=', 'categories.id')
//                    ->join('users', 'posts.user_id', '=', 'users.id')
//                    ->whereIn('posts.id', $posts)
//                    ->select('posts.*', 'categories.category_title', 'categories.category_title_clean' ,'users.name')
//                    ->paginate($perPage);
//
//                $this->strimContent($items, $length);
//
//                return $items;
            }
        }


        switch ($sort){
            case 'time':
                $orderBy = 'post_timer';
                break;
            case 'view':
                $orderBy = 'post_view';
                break;
            case 'like':
                $orderBy = 'post_like';
                break;
            default:
                $orderBy = 'post_timer';
        }

        $posts = Post::orderBy($orderBy, 'desc')
        ->where([
            ['posts.post_status', 1],
            ['posts.post_timer', '<=', $now]
        ]);

        if (!empty($user))
        {
            $posts = $posts->where('posts.user_id', $user->id);
        }

        if (!empty($category))
        {
            $posts = $posts->where('posts.category_id', $category->id);
        }

        if (!empty($tag))
        {
            $posts = $posts->whereIn('posts.id', $postsAr);
        }

        if (!empty($username) && empty($user))
        {
            return null;
        }

        $posts = $posts->join('categories', 'posts.category_id', '=', 'categories.id')
            ->join('users', 'posts.user_id', '=', 'users.id')
            ->select('posts.*', 'categories.category_title', 'categories.category_title_clean' ,'users.name')
            ->paginate($perPage);

        $this->strimContent($posts, $length);

        return $posts;

    }

    /**
     * Get the user that owns the post.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /*public function postsOfUser($username, $perPage, $length)
    {
        $now = Carbon::now()->timestamp;
        $user = User::where('name', $username)->first();
        if ($user != null) {
            $items = Post::where([
                ['posts.post_status', 1],
                ['posts.user_id', $user->id],
                ['posts.post_timer', '<=', $now]
            ])
                ->join('categories', 'posts.category_id', '=', 'categories.id')
                ->join('users', 'posts.user_id', '=', 'users.id')
                ->select('posts.*', 'categories.category_title', 'categories.category_title_clean' ,'users.name')
                ->paginate($perPage);

            $this->strimContent($items, $length);

            return $items;
        }
        else
        {
            abort(404);
        }
    }*/

    /**
     * Get list
     */
    /*public function getListOld($perPage, $length)
    {
        $now = Carbon::now()->timestamp;
        $items = $this::orderBy('post_timer', 'asc')
            ->where([
                ['posts.post_status', 1],
                ['posts.post_timer', '<=', $now]
            ])
            ->join('categories', 'posts.category_id', '=', 'categories.id')
            ->join('users', 'posts.user_id', '=', 'users.id')
            ->select('posts.*', 'categories.category_title', 'categories.category_title_clean' ,'users.name')
            ->paginate($perPage);

        $this->strimContent($items, $length);

        return $items;
    }*/

    /**
     * Related post
     */

    public function relatedPost($id, $categoryId)
    {
        $items = $this::orderBy('post_timer', 'asc')
            ->where('category_id', $categoryId)
            ->where('id', '<>', $id)
            ->take(10)
            ->get();

        return $items;
    }

    /**
     * Search post
     */
    public function searchPost($searchKey, $perPage, $length)
    {
        $items = $this::orderBy('id', 'asc')
            ->where('posts.post_title_clean', 'like', "%$searchKey%")
            ->join('categories', 'posts.category_id', '=', 'categories.id')
            ->select('posts.*', 'categories.category_title_clean')
            ->paginate($perPage);

        $this->strimContent($items, $length);

        return $items;
    }

    /**
     * Strim content
     */
    public function strimContent($items, $length)
    {
        foreach ($items as $item){
            $item->post_content = strip_tags($item->post_content);
            $n = strlen($item->post_content);
            if ($n > $length)
            {
                $item->post_content = substr($item->post_content, 0, $length) . '...';
            }
        }

        return $items;
    }

}
