<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Cv extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'career_id',
        'cv_title',
        'cv_title_clean',
        'cv_thumbnail',
        'cv_address',
        'cv_phone',
        'cv_email',
        'cv_skype',
        'cv_personal_summary',
        'cv_work_experience',
        'cv_skill',
        'cv_academic',
        'cv_status_public'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    /**
     * Get the category that owns the post.
     */
    public function user()
    {
        return $this->belongsTo('App\Company');
    }

    public function career()
    {
        return $this->belongsTo('App\Career');
    }

    public function searchCv($searchKey, $perPage, $length)
    {

        $items = $this::orderBy('id', 'asc')
            ->where('cvs.cv_title_clean', 'like', "%$searchKey%")
            ->where('cvs.	cv_status_public', 1)
            ->join('users', 'cvs.user_id', '=', 'users.id')
            ->join('careers', 'cvs.career_id', '=', 'careers.id')
            ->select('cvs.*', 'users.name', 'careers.career_title')
            ->paginate($perPage);

        foreach ($items as $item){
            $item->cv_work_experience = strip_tags($item->cv_work_experience);
            $n = strlen($item->cv_work_experience);
            if ($n > $length)
            {
                $item->cv_work_experience = substr($item->cv_work_experience, 0, $length) . '...';
            }
            else
            {
                $item->cv_work_experience = substr($item->cv_work_experience, 0, $length);
            }
        }

        return $items;
    }

    public function myCvs()
    {
        $cvs = $this::orderBy('created_at', 'asc')
                    ->where('user_id', Auth::id())
                    ->paginate(25);

        return $cvs;
    }
}
