<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Career extends Model
{
    protected $fillable = [
        'user_id',
        'career_title',
        'career_title_clean',
    ];

    public function getList()
    {
        return $this::all();
    }

    public function jobes()
    {
        return $this->hasMany('App\Job');
    }
}
