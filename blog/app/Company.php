<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Job;

class Company extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'company_title',
        'company_title_clean',
        'company_thumbnail',
        'company_address',
        'company_website',
        'company_email',
        'company_size',
        'company_overview'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    /**
     * Get the category that owns the post.
     */
    public function jobs()
    {
        return $this->hasMany('App\Job');
    }

    public function currentJobs($id)
    {
        $items = Job::orderBy('jobs.created_at', 'desc')
            ->where('jobs.company_id', $id)
            ->whereDate('jobs.job_expired', '>', Carbon::today()->toDateString())
            ->join('companies', 'jobs.company_id', '=', 'companies.id')
            ->select('jobs.*', 'companies.company_thumbnail')
            ->paginate(24);
        return $items;
    }

    public function searchCompany($searchKey, $perPage, $length)
    {
        $items = $this::orderBy('id', 'asc')
            ->where('companies.company_title_clean', 'like', "%$searchKey%")
            ->select('companies.*')
            ->paginate($perPage);

        foreach ($items as $item){
            $item->company_overview = strip_tags($item->company_overview);
            $n = strlen($item->company_overview);
            if ($n > $length)
            {
                $item->company_overview = substr($item->company_overview, 0, $length) . '...';
            }
            else
            {
                $item->company_overview = substr($item->company_overview, 0, $length);
            }
        }

        return $items;
    }

    public function getList($perPage, $length, $auth = null)
    {
        if (!empty($auth))
        {
            $companies = $this::orderBy('companies.created_at', 'desc')
                ->where('user_id', $auth->id)
                ->select('companies.*')
                ->paginate($perPage);
        }
        else
        {
            $companies = $this::orderBy('companies.created_at', 'desc')
                ->select('companies.*')
                ->paginate($perPage);
        }


        foreach ($companies as $company)
        {
            $currentJobs = $this->currentJobs($company->id);
            $company->current_jobs = $currentJobs->count();
        }

        $this->stripContent($companies, $length);

        return $companies;
    }

    public function stripContent($companies, $length)
    {
        foreach ($companies as $company){
            $company->company_overview = strip_tags($company->company_overview);
            $n = strlen($company->company_overview);
            if ($n > $length)
            {
                $company->company_overview = substr($company->company_overview, 0, $length) . '...';
            }
        }

        return $companies;
    }
}
