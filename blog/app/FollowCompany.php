<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FollowCompany extends Model
{
    protected $fillable = [
        'user_id',
        'company_id'
    ];
}
