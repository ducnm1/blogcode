<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'group_title'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    /**
     * Get the group to action record associated with the group.
     */
    public function groupsAction()
    {
        return $this->hasOne('App\GroupsAction');
    }

    /**
     * Get the users for the blog group.
     */
    public function users()
    {
        return $this->hasMany('App\User');
    }
}
