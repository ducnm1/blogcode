<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use App\Cv;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the comments.
     */
    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    /**
     * Get the posts.
     */
    public function posts()
    {
        return $this->hasMany('App\Post');
    }

    /**
     * Get the group that owns the user.
     */
    public function group()
    {
        return $this->belongsTo('App\Group');
    }

    /**
     * Get the cvs.
     */
    public function cvs($userName)
    {
        $user = $this::where('name', $userName)->first();
        $cvs = Cv::where('cvs.user_id', $user->id)
                ->join('careers', 'cvs.career_id', 'careers.id')
                ->select('cvs.*', 'careers.career_title')
                ->get();

        return $cvs;
    }

    public function companies()
    {
        return $this->hasMany('App\Company');
    }
}
