<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;


class Job extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'company_id',
        'career_id',
        'job_title',
        'job_title_clean',
        'job_salary',
        'job_skill',
        'job_adress',
        'job_content',
        'job_experience',
        'job_benefit',
        'job_expired'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    /**
     * Get the category that owns the post.
     */
    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function career()
    {
        return $this->belongsTo('App\Career');
    }

    public function getList($perPage, $length, $auth = null)
    {
        if (!empty($auth))
        {
            $jobs = $this::orderBy('jobs.created_at', 'desc')
                ->where('jobs.user_id', $auth->id)
                ->whereDate('jobs.job_expired', '>', Carbon::today()->toDateString())
                ->join('companies', 'jobs.company_id', '=', 'companies.id')
                ->join('careers', 'jobs.career_id', '=', 'careers.id')
                ->select('jobs.*', 'companies.company_title', 'careers.career_title')
                ->paginate($perPage);
        }
        else
        {
            $jobs = $this::orderBy('jobs.created_at', 'desc')
                ->whereDate('jobs.job_expired', '>', Carbon::today()->toDateString())
                ->join('companies', 'jobs.company_id', '=', 'companies.id')
                ->join('careers', 'jobs.career_id', '=', 'careers.id')
                ->select('jobs.*', 'companies.company_thumbnail', 'careers.career_title')
                ->paginate($perPage);
        }


        $this->stripContent($jobs, $length);

        return $jobs;
    }

    public function relatedPosts($id, $number)
    {
        $jobs = $this::orderBy('jobs.created_at', 'desc')
            ->where('jobs.id', '<>', $id)
            ->whereDate('jobs.job_expired', '>', Carbon::today()->toDateString())
            ->join('companies', 'jobs.company_id', '=', 'companies.id')
            ->select('jobs.*', 'companies.company_thumbnail')
            ->take($number)
            ->get();

        $this->stripContent($jobs, 100);

        return $jobs;
    }

    public function stripContent($jobs, $length)
    {
        foreach ($jobs as $job){
            $job->job_content = strip_tags($job->job_content);
            $n = strlen($job->job_content);
            if ($n > $length)
            {
                $job->job_content = substr($job->job_content, 0, $length) . '...';
            }
        }

        return $jobs;
    }

    public function searchJob($searchKey, $perPage, $length)
    {
        $items = $this::orderBy('id', 'asc')
            ->where('jobs.job_title_clean', 'like', "%$searchKey%")
            ->join('companies', 'jobs.company_id', '=', 'companies.id')
            ->select('jobs.*', 'companies.company_thumbnail')
            ->paginate($perPage);

        foreach ($items as $item){
            $item->job_content = strip_tags($item->job_content);
            $n = strlen($item->job_content);
            if ($n > $length)
            {
                $item->job_content = substr($item->job_content, 0, $length) . '...';
            }
            else
            {
                $item->job_content = substr($item->job_content, 0, $length);
            }
        }

        return $items;
    }
}
