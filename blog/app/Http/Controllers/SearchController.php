<?php

namespace App\Http\Controllers;

use App\Cv;
use App\Post;
use App\Job;
use App\Company;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    use shareMethod;
    public function search(Request $request){
        $searchKeyOriginal = $request->searchKey;
        $searchKey = $this->EnToVn($request->searchKey);
        $searchIn = strtolower($request->searchIn);

        switch ($searchIn)
        {
            case 'blog':
                $post = new Post;
                $posts = $post->searchPost($searchKey, 24, 100);

                return view('pages.searches.index', ['posts'=> $posts, 'searchKey'=>$searchKeyOriginal, 'searchIn'=>$searchIn]);
                break;
            case 'cv':
                $cv = new Cv;
                $cvs = $cv->searchCv($searchKey, 24, 100);

                return view('pages.searches.index', ['cvs'=> $cvs, 'searchKey'=>$searchKeyOriginal, 'searchIn'=>$searchIn]);
                break;
            case 'job':
                $job = new Job;
                $jobs = $job->searchJob($searchKey, 24, 100);

                return view('pages.searches.index', ['jobs'=> $jobs, 'searchKey'=>$searchKeyOriginal, 'searchIn'=>$searchIn]);
                break;
            case 'company':
                $company = new Company;
                $companies = $company->searchCompany($searchKey, 24, 100);

                return view('pages.searches.index', ['companies'=> $companies, 'searchKey'=>$searchKeyOriginal, 'searchIn'=>$searchIn]);
                break;
            default:
                $items = [];
        }


    }
}
