<?php
/**
 * Created by PhpStorm.
 * User: ducnm
 * Date: 1/11/2017
 * Time: 10:07 AM
 */

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LogoutController extends Controller
{
    public function logout(){
        if (Auth::check())
        {
            Auth::logout();
        }

        return redirect('/');
    }
}

