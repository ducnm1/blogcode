<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

use App\Http\Requests;

use App\Category;

use Carbon\Carbon;

use Illuminate\Support\Facades\Auth;

use Symfony\Component\HttpKernel\EventListener\ValidateRequestListener;
class CategoryController extends Controller
{
    use shareMethod;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function apiGetList()
    {
        return Category::select('category_title','category_title_clean')->get()->toJson();
    }

    /**
     * Display a list item of for manager
     *
     * @return $posts
     */

    public function categoriesManagement() {
        $categories = Category::orderBy('id', 'asc')
            ->join('users', 'categories.user_id', '=', 'users.id')
            ->select('categories.*' ,'users.name')
            ->paginate(25);

        return view('pages.categories.management', ['categories'=> $categories]);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permission = $this->checkPermission('create_category');

        if ($permission)
        {
            $cats = Category::orderBy('id', 'asc')->select('id', 'category_title')->get()->toArray();
            $categories = array(0=>'none');
            for ($i= 0; $i < count($cats); $i++){
                $categories[$cats[$i]['id']] = $cats[$i]['category_title'];
            }
            return view('pages.categories.create-edit', ['categories'=>$categories]);
        }
        else
        {
            return abort(401);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $permission = $this->checkPermission('create_category');

        if ($permission)
        {
            $this->validate($request, [
                'category_title' => 'required|unique:categories|max:255'

            ]);

            $category = new Category;
            $category->user_id = Auth::user()->id;
            $category->category_title = trim($request->category_title);
            $category->category_title_clean = $this->EnToVn(trim($request->category_title));
            $category->category_description = trim($request->category_description);

            if ($request->has('category_parent_id') && $request->category_parent_id != 0) {
                $category->category_parent_id = $request->category_parent_id;
                $categoryParent = Category::find($request->category_parent_id);
                if ($categoryParent->category_level < 3) {
                    $category->category_level = $categoryParent->category_level + 1;
                } else {
                    $category->category_level = 3;
                }
            }

            $category->category_thumbnail = $request->category_thumbnail;
            $category->save();
            return redirect()->route('categoryEdit', ['id'=>$category->id]);
        }
        else
        {
            return abort(401);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $permission = $this->checkPermission('edit_category');

        if ($permission)
        {
            $category = Category::find($id);
            if (!empty($category))
            {
                $cats = Category::orderBy('id', 'asc')->select('id', 'category_title')->get()->toArray();
                $categories = array(0=>'none');
                for ($i= 0; $i < count($cats); $i++){
                    if ($cats[$i]['id'] != $category->id)
                    {
                        $categories[$cats[$i]['id']] = $cats[$i]['category_title'];
                    }
                }
                return view('pages.categories.create-edit', ['categories'=>$categories, 'category'=>$category]);
            }
            else
            {
                return abort(404);
            }
        }
        else
        {
            return abort(401);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $permission = $this->checkPermission('edit_category');

        if ($permission)
        {
            $category = Category::find($id);
            $category->category_title = trim($request->category_title);
            $category->category_title_clean = $this->EnToVn(trim($request->category_title));
            $category->category_description = trim($request->category_description);

            if ($request->has('category_parent_id') && $request->category_parent_id != 0) {
                $category->category_parent_id = $request->category_parent_id;
                $categoryParent = Category::find($request->category_parent_id);
                if ($categoryParent->category_level < 3) {
                    $category->category_level = $categoryParent->category_level + 1;
                } else {
                    $category->category_level = 3;
                }
            }


            $category->category_thumbnail = $request->category_thumbnail;

            $category->save();
            return redirect()->route('categoryEdit', ['id'=>$category->id]);
        }
        else
        {
            return abort(401);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $permission = $this->checkPermission('edit_category');

        if ($permission)
        {
            $category = Category::find($id);
            $category->destroy($id);
            // reset category_id in posts table
            $posts = Post::orderBy('id', 'asc')->where('category_id', $id)->select('posts.id')->get()->toArray();

            for ($i=0; $i < count($posts); $i++){
                $post = Post::find($posts[$i]['id']);
                $post->category_id = 0;
                $post->save();
            }

            return redirect()->route('categoriesManagement');
        }
        else
        {
            return abort(401);
        }
    }
}
