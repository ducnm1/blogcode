<?php

namespace App\Http\Controllers;

use App\FollowCompany;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FollowCompanyController extends Controller
{
    public function create(Request $request)
    {
        $follow = new FollowCompany;
        $follow->user_id = Auth::id();
        $follow->company_id = $request->company_id;
        $follow->save();
    }

    public function destroy(Request $request)
    {
        $follow = FollowCompany::where('company_id',$request->company_id)->first();
        $follow->destroy($follow->id);
    }
}
