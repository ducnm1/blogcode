<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\EventListener\ValidateRequestListener;
use App\Cv;
use App\Career;

class CvController extends Controller
{
    use shareMethod;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function management()
    {
        $cv = new Cv;
        $cvs = $cv->myCvs();

        return view('pages.cvs.management', ['cvs'=>$cvs]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $career = new Career;
        $careers = $career->getList();
        $careerAr = array();

        foreach ($careers as $career)
        {
            $careerAr[$career->id] = $career->career_title;
        }
        return view('pages.cvs.create-edit', ['careers'=>$careerAr]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'cv_title' => 'required|max:255',
        ]);

        $cv = new Cv;

        $cv->user_id = Auth::id();
        $cv->career_id = $request->career_id;
        $cv->cv_title = $request->cv_title;
        $cv->cv_title_clean = $this->EnToVn($cv->cv_title);
        $cv->cv_thumbnail = Auth::user()->avatar;
        $cv->cv_address = $request->cv_address;
        $cv->cv_phone = $request->cv_phone;
        $cv->cv_skype = $request->cv_skype;
        $cv->cv_email = $request->cv_email;
        $cv->cv_personal_summary = $request->cv_personal_summary;
        $cv->cv_work_experience = $request->cv_work_experience;
        $cv->cv_skill = $request->cv_skill;
        $cv->cv_academic = $request->cv_academic;
        $request->cv_status_public !== null ? $cv->cv_status_public = 1 : $cv->cv_status_public = 0;

        $cv->save();

        return redirect()->route('cvsManagent');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($titleClean, $id)
    {
        $id = explode('-', $id);
        $idCv = $id[count($id) - 1];
        unset($id[count($id) - 1]);
        $id = implode('-', $id);
        $titleClean = $titleClean . '-' . $id;

        $cv = Cv::find($idCv);
        $cv->career_title = $cv->career->career_title;

        return view('pages.cvs.index', ['cv'=>$cv]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cv = Cv::find($id);
        if (!empty($cv))
        {
            $career = new Career;
            $careers = $career->getList();
            $careerAr = array();

            foreach ($careers as $career)
            {
                $careerAr[$career->id] = $career->career_title;
            }
            return view('pages.cvs.create-edit', ['cv'=>$cv, 'careers'=>$careerAr]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'cv_title' => 'required|max:255',
        ]);

        $cv = Cv::find($id);

        if ($cv->user_id == Auth::id())
        {
            $cv->user_id = Auth::id();
            $cv->career_id = $request->career_id;
            $cv->cv_title = $request->cv_title;
            $cv->cv_title_clean = $this->EnToVn($cv->cv_title);
            $cv->cv_thumbnail = Auth::user()->avatar;
            $cv->cv_address = $request->cv_address;
            $cv->cv_phone = $request->cv_phone;
            $cv->cv_skype = $request->cv_skype;
            $cv->cv_email = $request->cv_email;
            $cv->cv_personal_summary = $request->cv_personal_summary;
            $cv->cv_work_experience = $request->cv_work_experience;
            $cv->cv_skill = $request->cv_skill;
            $cv->cv_academic = $request->cv_academic;
            $request->cv_status_public !== null ? $cv->cv_status_public = 1 : $cv->cv_status_public = 0;

            $cv->save();

            return redirect()->route('cvsManagement');
        }
        else
        {
            return abort(401);
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cv = Cv::find($id);
        if ($cv->user_id == Auth::id())
        {
            $cv->destroy($id);
            return redirect()->route('cvsManagement');
        }
        else
        {
            return abort(401);
        }
    }

    public function applyCv(Request $request)
    {
        $applyCv = new ApplyCv;
        $applyCv->user_id = Auth::id();
        $applyCv->cv_id = $request->cv_id;
        $applyCv->company_id = $request->company_id;
        $applyCv->save();

        return redirect()->route('jobList');
    }
}
