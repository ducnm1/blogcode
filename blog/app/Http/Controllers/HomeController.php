<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Job;
use App\Company;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    use shareMethod;
    public function index()
    {
        $post = new Post;
        $posts = $post->getList('', '', '', '', 9, 100);

        $job = new Job;
        $jobs = $job->getList(9, 100);

        $company = new Company;
        $companies = $company->getList(9, 100);

        return view('pages.index',
            [
                'posts'=> $posts,
                'jobs'=>$jobs,
                'companies'=>$companies
            ]
        );
    }
}
