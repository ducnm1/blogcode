<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Post;

use App\Category;

use App\Action;

use App\GroupsAction;

use App\User;

use App\Tag;

use Carbon\Carbon;

use Illuminate\Support\Facades\Auth;

use Symfony\Component\HttpKernel\EventListener\ValidateRequestListener;

use Goutte\Client;

class PostController extends Controller
{
    use shareMethod;

    /**
     * Display a listing of the resource.
     *
     * @return $items
     */
    public function index()
    {
        dd('die');
        $post = new Post;
        $posts = $post->getList(24, 100);
        return view('pages.posts.list', ['posts'=> $posts]);
    }

//    public function apiIndex()
//    {
//
//        $now = Carbon::now()->timestamp;
//        $items = Post::orderBy('post_timer', 'asc')
//            ->where([
//                ['posts.post_status', 1],
//                ['posts.post_timer', '<=', $now]
//            ])
//            ->join('categories', 'posts.category_id', '=', 'categories.id')
//            ->select('posts.*', 'categories.category_title_clean')
//            ->paginate(24);
//
//        foreach ($items as $item){
//            $item->post_content = strip_tags($item->post_content);
//            $n = strlen($item->post_content);
//            if ($n > 100)
//            {
//                $item->post_content = substr($item->post_content, 0, 100) . '...';
//            }
//            else
//            {
//                $item->post_content = substr($item->post_content, 0, 100);
//            }
//        }
//
//        return $items;
//    }

    /**
     * Display a list item of a category
     *
     * @return $items
     */
    public function postsOfCategory(Request $request)
    {
        $category = !empty($request->category)?$request->category:'';
        //$username = !empty($request->username)?$request->username:'';
        $sort = !empty($request->sort) ? $request->sort : 'time';

        $post = new Post;
        $posts = $post->getList($category, '', '', $sort, 24, 100);

        $categories = Category::orderBy('id')->select('id', 'category_title', 'category_title_clean')->get();
        return view('pages.posts.list', ['posts' => $posts, 'categories'=>$categories, 'categoryR'=> $category, 'sort'=>$sort]);
    }

    /**
     * Display a list item of a category
     *
     * @return $items
     */
    public function postsOfUser($username)
    {
        $post = new Post;
        $posts = $post->getList('', '', $username,'', 24, 100);
        return view('pages.posts.list', ['posts' => $posts]);
    }

    public function postsOfTag($tag){
        $post = new Post;
        $posts = $post->getList('', $tag, '', '', 24, 100);
        return view('pages.posts.list', ['posts'=> $posts]);
    }
    /**
     * Display a list item of for manager
     *
     * @return $posts
     */

    public function management() {
        $group_title = Auth::user()->group->group_title;
        $post = new Post;

        if (in_array($group_title, ['superadmin', 'admin']))
        {
            $posts = $post->getList(24, 100);
        }
        else
        {
            $posts = $post->getList('', '', Auth::user()->name,'', 24, 100);
        }

        foreach ($posts as $post){
            $post->post_timer = date('m/d/Y H:i:s', $post->post_timer);
        }

        return view('pages.posts.management', ['posts'=> $posts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //$permission = $this->checkPermission('create_post');

        if ( Auth::user()->status ) {
            $cats = Category::orderBy('id', 'asc')->select('id', 'category_title')->get()->toArray();
            $categories = array();
            for ($i= 0; $i < count($cats); $i++){
                $categories[$cats[$i]['id']] = $cats[$i]['category_title'];
            }
            return view('pages.posts.create-edit', ['categories'=>$categories]);
        }
        else
        {
            return abort(401);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$permission = $this->checkPermission('create_post');

        if (Auth::user()->status)
        {
            $this->validate($request, [
                'post_title' => 'required|unique:posts|max:255',
                'post_content' => 'required'
            ]);

            $post = new Post;
            $post->user_id = Auth::id();
            $post->post_title = trim($request->post_title);
            $post->post_title_clean = $this->EnToVn($request->post_title);
            $post->post_content = trim($request->post_content);
            $post->category_id = $request->category_id;
            $request->post_status != null ? $post->post_status = 1 : $post->post_status = 0;
            $request->post_comment_status != null ? $post->post_comment_status = 1 : $post->post_comment_status = 0;
            $post->post_timer = Carbon::parse($request->post_timer)->timestamp ;

            if ($request->hasFile('post_thumbnail') && $request->file('post_thumbnail')->isValid()) {
                $post->post_thumbnail = $this->uploadImage($request, 'post_thumbnail');
            }
            else
            {
                $post->post_thumbnail = '';
            }

            $post->save();

            // add tags
            $postTags = explode(",", trim($request->post_tags));

            foreach ($postTags as $key => $value)
            {
                $tag = new Tag;
                $tag->post_id = $post->id;
                $tag->tag_title = trim($value);
                $tag->tag_title_clean = $this->EnToVn($tag->tag_title);
                $tag->save();
            }

            return redirect()->route('postEdit', ['id'=>$post->id]);
        }
        else
        {
            return abort(401);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($titleClean, $id)
    {
        $id = explode('-', $id);
        $idPost = $id[count($id) - 1];
        unset($id[count($id) - 1]);
        $id = implode('-', $id);
        $titleClean = $titleClean . '-' . $id;

        $item = Post::find($idPost);

        if (!empty($item))
        {
            if ($titleClean == $item->post_title_clean)
            {
                $item->post_view ++;
            
                $item->save();

                $category = $item->category;
                $user = $item->user;


                $item->user_name = $user->name;

                $itemsRelated = $item->relatedPost($item->id, $item->category_id);

                $item->post_timer = date('H:i m-d-Y', $item->post_timer);
                //$item->post_timer = date('H:i:s m/d/Y', $item->post_timer);

                $item->tags = $item->tags($item->id);

                $breadcrumb = $this->breadcrumb($item->category_id);

                return view('pages.posts.detail', [
                    'item' => $item,
                    'itemsRelated' => $itemsRelated,
                    'breadcrumb' => $breadcrumb
                ]);
            }
            else
            {
                return redirect()->route('postDetail', [
                    'titleClean'=>$item->post_title_clean, 'id'=>$idPost
                ]);
            }
        }
        else
        {
            return abort(404);
        }
    }

    public function apiShow($id)
    {
        $item = Post::find($id);

        if ($item != null) 
        {
            $itemsRelated = Post::orderBy('post_timer', 'asc')->where('category_id', $item->category_id)->get();
            $item->itemsRelated = $itemsRelated;
        }
        $item->post_like = count($item->likes);
        $item = $item->toJson();

        return $item;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //$permission = $this->checkPermission('edit_post');

        if (Auth::user()->status)
        {
            $post = Post::find($id);
            $group_title = Auth::user()->group->group_title;

            if (
                !empty($post) &&
                (
                    Auth::user()->id == $post->user_id ||
                    in_array($group_title, ['superadmin', 'admin'])
                )
            )
            {
                $post->post_timer = $this->timestampToDateTime($post->post_timer);
                $cats = Category::orderBy('id', 'asc')->select('id', 'category_title')->get()->toArray();
                $categories = array();
                for ($i= 0; $i < count($cats); $i++){
                    $categories[$cats[$i]['id']] = $cats[$i]['category_title'];
                }

                $tags = Tag::orderBy('tag_title')->where('post_id', $id)->get();
                $postTags = array();
                foreach ($tags as $tag)
                {
                    array_push($postTags, $tag->tag_title_clean);
                }
                $post->post_tags = implode(",", $postTags);

                return view('pages.posts.create-edit', ['categories'=>$categories, 'post'=>$post]);
            }
            else
            {
                return abort(404);
            }
        }
        else
        {
            return abort(401);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //$permission = $this->checkPermission('edit_post');

        if (Auth::user()->status)
        {
            $post = Post::find($id);
            $group_title = Auth::user()->group->group_title;

            if (
                !empty($post) &&
                (
                    Auth::user()->id == $post->user_id ||
                    in_array($group_title, ['superadmin', 'admin'])
                )
            )
            {
                $post->post_title = trim($request->post_title);
                $post->post_title_clean = $this->EnToVn(trim($request->post_title));
                $post->post_content = trim($request->post_content);
                $post->category_id = $request->category_id;
                $request->post_status != null ? $post->post_status = 1 : $post->post_status = 0;

                $request->post_comment_status != null ? $post->post_comment_status = 1 : $post->post_comment_status = 0;

                $post->post_timer = Carbon::parse($request->post_timer)->timestamp ;

                //$post->post_thumbnail = $request->post_thumbnail;
                if ($request->hasFile('post_thumbnail') && $request->file('post_thumbnail')->isValid()) {
                    $post->post_thumbnail = $this->uploadImage($request, 'post_thumbnail');
                }

                $post->save();

                $tags = Tag::orderBy('tag_title')->where('post_id', $id)->get();
                foreach ($tags as $tag)
                {
                    $tag = Tag::find($tag->id);
                    $tag->destroy($tag->id);
                }

                $postTags = explode(",", trim($request->post_tags));

                foreach ($postTags as $key => $value)
                {
                    $tag = new Tag;
                    $tag->post_id = $post->id;
                    $tag->tag_title = trim($value);
                    $tag->tag_title_clean = $this->EnToVn($tag->tag_title);
                    $tag->save();
                }
                return redirect()->route('postEdit', ['id'=>$post->id]);
            }
            else
            {
                return abort(404);
            }
        }
        else
        {
            return abort(401);
        }
    }

    public function apiView($id)
    {
        $post = Post::find($id);

        if (!empty($post)) {
            $post->post_view ++;
            
            $post->save();
        }
        return '{}';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //$permission = $this->checkPermission('delete_post');
        $post = Post::find($id);
        if (Auth::user()->status)
        {
            $group_title = Auth::user()->group->group_title;

            if (
                !empty($post) &&
                (
                    Auth::user()->id == $post->user_id ||
                    in_array($group_title, ['superadmin', 'admin'])
                )
            )
            {
                $post->destroy($id);
                return redirect()->route('postsManagement');
            }
            else
            {
                return abort(404);
            }
        }
        else
        {
            return abort(401);
        }
    }

    public function crawlerData()
    {
        $client = new Client();
        $crawler = $client->request('GET', 'http://phpcoban.com/curl-trong-php/');

        $crawler->filter('.entry-inner')->each(function ($node) {
            //var_dump($node->text())."\n";
            $node->filter('div')->each(function ($node){
                if ($node->attr('class') == 'ezAdsense')
                {
                    echo 123123;
                }
            });
        });
        dd('die');
    }
}
