<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Option;

use Illuminate\Support\Facades\Auth;

use Symfony\Component\HttpKernel\EventListener\ValidateRequestListener;

use Image;

class OptionController extends Controller
{
    use shareMethod;
    /**
     * Show the form for editing the specified resource.
     *
     * @param
     * @return \Illuminate\Http\Response
     */

    public function edit()
    {
        $permission = $this->checkPermission('option');

        if ( $permission ) {
            $option = Option::orderBy('id', 'asc')->first();
            return view('pages.options.option', ['option'=>$option]);
        }
        else
        {
            return abort(401);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        $permission = $this->checkPermission('option');

        if ($permission)
        {
            $option = Option::find($id);
            if (!empty($option))
            {
                $option->option_person_contact = trim($request->option_person_contact);
                $option->option_skype = trim($request->option_skype);
                $option->option_yahoo = trim($request->option_yahoo);
                $option->option_mail = trim($request->option_mail);
                $option->option_facebook = trim($request->option_facebook);
                $option->option_twitter = trim($request->option_twitter);
                $option->option_youtube = trim($request->option_youtube);
                $option->option_address_1 = trim($request->option_address_1);
                $option->option_address_2 = trim($request->option_address_2);
                $option->option_address_3 = trim($request->option_address_3);
                $option->option_mobile_phone = trim($request->option_mobile_phone);
                $option->option_home_phone = trim($request->option_home_phone);
                $option->option_map_latitude = trim($request->option_map_latitude);
                $option->option_map_longitude = trim($request->option_map_longitude);

                if ($request->hasFile('option_logo_big') && $request->file('option_logo_big')->isValid()) {
                    $option->option_favicon = $this->uploadImage($request, 'option_logo_big');
                }

                if ($request->hasFile('option_logo_small') && $request->file('option_logo_small')->isValid()) {
                    $option->option_favicon = $this->uploadImage($request, 'option_logo_small');
                }

                if ($request->hasFile('option_favicon') && $request->file('option_favicon')->isValid()) {
                    $option->option_favicon = $this->uploadImage($request, 'option_favicon', '400:300');
                }

                $request->option_pager != null ? $option->option_pager = 1 : $option->option_pager = 0;
                $request->option_breadcrumb != null ? $option->option_breadcrumb = 1 : $option->option_breadcrumb = 0;
                $request->option_viewer != null ? $option->option_viewer = 1 : $option->option_viewer = 0;
                $request->option_like != null ? $option->option_like = 1 : $option->option_like = 0;

                $option->save();
                return redirect()->route('home');
            }
            else
            {
                return abort(404);
            }
        }
        else
        {
            return abort(401);
        }
    }
}
