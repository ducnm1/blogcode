<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Carbon\Carbon;
use App\Job;
use App\Company;
use App\User;
use App\Career;
use Symfony\Component\HttpKernel\EventListener\ValidateRequestListener;
use Illuminate\Support\Facades\Auth;

class JobController extends Controller
{
    use shareMethod;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $job = new Job;
        $jobs = $job->getList(24, 100);

        return view('pages.jobs.list', ['jobs'=> $jobs]);
    }

    public function management()
    {
        $job = new Job;
        $jobs = $job->getList(24, 100, Auth::user());

        return view('pages.jobs.management', ['jobs'=> $jobs]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Career
        $career = new Career;
        $careers = $career->getList();
        $careerAr = array();

        foreach ($careers as $career)
        {
            $careerAr[$career->id] = $career->career_title;
        }

        // Company
        $companies = Auth::user()->companies;
        $companyAr = array();

        foreach ($companies as $company)
        {
            $companyAr[$company->id] = $company->company_title;
        }

        return view('pages.jobs.create-edit',['careers'=>$careerAr, 'companies'=>$companyAr]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $this->validate($request, [
            'job_title' => 'required|max:255',
        ]);

        $job = new Job;

        $job->user_id = Auth::id();
        $job->career_id = $request->career_id;
        $job->company_id = $request->company_id;
        $job->job_title = $request->job_title;
        $job->job_title_clean = $this->EnToVn($job->job_title_clean);
        $job->job_salary = $request->job_salary;
        $job->job_skill = $request->job_skill;
        $job->job_address = $request->job_address;
        $job->job_content = $request->job_content;
        $job->job_experience = $request->job_experience;
        $job->job_benefit = $request->job_benefit;
        $job->job_expired = $request->job_expired;

        $job->save();

        return redirect()->route('jobsManagement');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($titleClean, $id)
    {

        $id = explode('-', $id);
        $idJob = $id[count($id) - 1];
        unset($id[count($id) - 1]);
        $id = implode('-', $id);
        $titleClean = $titleClean . '-' . $id;

        $item = Job::find($idJob);

//        var_dump($item);
//        dd('die');


        if (!empty($item))
        {
            $company = Company::find($item->company_id);
            $currentJobs = $company->currentJobs($item->company_id);
            $company->currentJobs = $currentJobs->count();
            $relatedJobs = $item->relatedPosts($idJob, 10);

            if (Auth::check())
            {
                $cvs = Auth::user()->cvs(Auth::user()->name);
                $x = array();
                foreach ($cvs as $cv)
                {
                    $x[$cv->id]= $cv->cv_title;
                }
                $cvs = $x;

                return view('pages.jobs.detail', [
                    'item' => $item,
                    'company' => $company,
                    'relatedJobs' => $relatedJobs,
                    'cvs' => $cvs
                ]);
            }

            return view('pages.jobs.detail', [
                'item' => $item,
                'company' => $company,
                'relatedJobs' => $relatedJobs
            ]);
        }
        else
        {
            return abort(404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $job = Job::find($id);

        if (!empty($job) && Auth::id() == $job->user_id)
        {
            // Career
            $career = new Career;
            $careers = $career->getList();
            $careerAr = array();

            foreach ($careers as $career)
            {
                $careerAr[$career->id] = $career->career_title;
            }

            // Company
            $companies = Auth::user()->companies;
            $companyAr = array();

            foreach ($companies as $company)
            {
                $companyAr[$company->id] = $company->company_title;
            }

            return view('pages.jobs.create-edit',['job'=>$job, 'careers'=>$careerAr, 'companies'=>$companyAr]);
        }
        else
        {
            return abort(404);
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'job_title' => 'required|max:255',
        ]);

        $job = Job::find($id);

        if (!empty($job) && Auth::id() == $job->user_id)
        {
            $job->user_id = Auth::id();
            $job->career_id = $request->career_id;
            $job->company_id = $request->company_id;
            $job->job_title = $request->job_title;
            $job->job_title_clean = $this->EnToVn($job->job_title_clean);
            $job->job_salary = $request->job_salary;
            $job->job_skill = $request->job_skill;
            $job->job_address = $request->job_address;
            $job->job_content = $request->job_content;
            $job->job_experience = $request->job_experience;
            $job->job_benefit = $request->job_benefit;
            $job->job_expired = $request->job_expired;

            $job->save();

            return redirect()->route('jobsManagement');
        }
        else
        {
            return abort(404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $job = Job::find($id);
        if (!empty($job) && $job->user_id == Auth::id())
        {
            $job->destroy($id);
            return redirect()->route('jobsManagement');
        }
        else
        {
            return abort(401);
        }
    }
}
