<?php

namespace App\Http\Controllers;

use App\FollowUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FollowUserController extends Controller
{
    public function create(Request $request)
    {
        $follow = new FollowUser;
        $follow->user_id = Auth::id();
        $follow->user_followed_id = $request->user_id;
        $follow->save();
    }

    public function destroy(Request $request)
    {
        $follow = FollowUser::where('user_followed_id',$request->user_followed_id)->first();
        $follow->destroy($follow->id);
    }
}
