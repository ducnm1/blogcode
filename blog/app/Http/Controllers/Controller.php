<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\Action;

use App\GroupsAction;

use App\User;

use App\Option;

use App\Category;

use Illuminate\Support\Facades\Auth;

use Image;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


}

trait shareMethod {

    public function EnToVn($str)
    {
        if(!$str) return false;
        $unicode = array(
            'a'=>array('á','à','ả','ã','ạ','ă','ắ','ặ','ằ','ẳ','ẵ','â','ấ','ầ','ẩ','ẫ','ậ'),
            'A'=>array('Á','À','Ả','Ã','Ạ','Ă','Ắ','Ặ','Ằ','Ẳ','Ẵ','Â','Ấ','Ầ','Ẩ','Ẫ','Ậ'),
            'd'=>array('đ'),
            'D'=>array('Đ'),
            'e'=>array('é','è','ẻ','ẽ','ẹ','ê','ế','ề','ể','ễ','ệ'),
            'E'=>array('É','È','Ẻ','Ẽ','Ẹ','Ê','Ế','Ề','Ể','Ễ','Ệ'),
            'i'=>array('í','ì','ỉ','ĩ','ị'),
            'I'=>array('Í','Ì','Ỉ','Ĩ','Ị'),
            'o'=>array('ó','ò','ỏ','õ','ọ','ô','ố','ồ','ổ','ỗ','ộ','ơ','ớ','ờ','ở','ỡ','ợ'),
            'O'=>array('Ó','Ò','Ỏ','Õ','Ọ','Ô','Ố','Ồ','Ổ','Ỗ','Ộ','Ơ','Ớ','Ờ','Ở','Ỡ','Ợ'),
            'u'=>array('ú','ù','ủ','ũ','ụ','ư','ứ','ừ','ử','ữ','ự'),
            'U'=>array('Ú','Ù','Ủ','Ũ','Ụ','Ư','Ứ','Ừ','Ử','Ữ','Ự'),
            'y'=>array('ý','ỳ','ỷ','ỹ','ỵ'),
            'Y'=>array('Ý','Ỳ','Ỷ','Ỹ','Ỵ'),
            '-'=>array(' ','&quot;','.','-–-')
        );
        foreach($unicode as $nonUnicode=>$uni){
            foreach($uni as $value)
                $str = @str_replace($value,$nonUnicode,$str);
            $str = preg_replace("/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'| |\"|\&|\#|\[|\]|~|$|_/","-",$str);
            $str = preg_replace("/-+-/","-",$str);
            $str = preg_replace("/^\-+|\-+$/","",$str);
        }
        return strtolower($str);
    }

    public function get_client_ip()
    {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

    public function timestampToDateTime($timestamp)
    {
        return date('m/d/Y H:i:s', $timestamp);
    }

    public function checkPermission($permission)
    {
        $userGroupId = Auth::user()->group_id;
        $groupsActions = GroupsAction::where('group_id', $userGroupId)->select('group_id', 'action_id')->get()->toArray();
        $actions = array();

        for ($i = 0; $i < count($groupsActions); $i++) {
            array_push($actions, $groupsActions[$i]['action_id']);
        }

        $action = Action::where('action_title', $permission)->select('id')->first();

        if (in_array($action->id, $actions))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function uploadImage($request, $fieldName, $size = null)
    {
        $target_dir = "upload/images/";
        $file = $request->file($fieldName);
        $file_name = time()."-".$file->getClientOriginalName();
        $file_extension =  $file->guessExtension();

        if(in_array($file_extension, array('jpg','png','jpeg', 'gif'))) {
            $file->move($target_dir, $file_name);
            $target_file = $target_dir.$file_name;
            if (!empty($size))
            {
                $size = explode(":", $size);
                $image = Image::make($target_file)->resize($size[0], $size[1])->save();
            }

            return "/" . $target_file;
        }
    }

    public function getOption()
    {
        return Option::first();
    }

    public function breadcrumb($catId)
    {
        $cat = Category::find($catId);
        $arCats = array();
        while ($cat != null) {
            $ar = array(
                'id'=>$cat->id,
                'category_title'=>$cat->category_title,
                'category_title_clean'=>$cat->category_title_clean
            );
            array_push($arCats, $ar);
            $cat = Category::find($cat->category_parent_id);

        }
        $arCats = array_reverse($arCats);
        return $arCats;
    }
}
