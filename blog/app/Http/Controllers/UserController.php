<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Post;
use App\Cv;
use App\FollowUser;
use App\Group;
use App\Action;
use App\GroupsAction;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    use shareMethod;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($username)
    {
        $user = User::where('name', $username)->first();

        $post = new Post;
        $posts = $post->getList('',$username,'', 24, 100);

        $cvs = $user->cvs($username);

        if (Auth::check())
        {
            $follow = FollowUser::where('user_id', Auth::id())
                ->where('user_followed_id', $user->id)->get();

            $statusFollow=count($follow);

            return view('pages.users.index', ['posts' => $posts, 'user'=>$user, 'cvs'=>$cvs, 'statusFollow'=>$statusFollow]);
        }

        return view('pages.users.index', ['posts' => $posts, 'user'=>$user, 'cvs'=>$cvs]);


    }
    public function management()
    {
        $permission = $this->checkPermission('edit_user');
        $groupTitle = Auth::user()->group->group_title;

        if ($permission)
        {
            $items = User::orderBy('id')
                ->join('groups', 'users.group_id', '=', 'groups.id')
                ->select('users.id', 'groups.group_title', 'users.name', 'users.status')
                ->paginate(24);

            return view('pages.users.list', ['items'=> $items]);
        }
        else
        {
            return abort(401);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $permission = $this->checkPermission('edit_user');

        if ($permission && $id != Auth::id())
        {
            $user = User::find($id);

            $groupTitleLogin = Auth::user()->group->group_title;
            $groupTitleNotLogin = $user->group->group_title;

            if ($groupTitleLogin == 'superadmin' && $groupTitleNotLogin == 'superadmin')
            {
                return abort(401);
            }
            elseif ($groupTitleLogin == 'admin' && ($groupTitleNotLogin == 'admin' || $groupTitleNotLogin == 'superadmin'))
            {
                return abort(401);
            }
            else
            {
                if (!empty($user))
                {
                    $groups = Group::orderBy('id')->select('id', 'group_title')->get()->toArray();
                    $groupsTitle = array();

                    foreach ($groups as $key => $value){
                        $groupsTitle[$value['id']] = $value['group_title'];
                    }
                    return view('pages.users.edit', ['user' => $user, 'groupsTitle' => $groupsTitle]);
                }
                else
                {
                    return abort(404);
                }
            }
        }
        else
        {
            return abort(401);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $permission = $this->checkPermission('edit_user');

        if ($permission)
        {
            $user = User::find($id);

            $groupTitleLogin = Auth::user()->group->group_title;
            $groupTitleNotLogin = $user->group->group_title;

            if ($groupTitleLogin == 'superadmin' && $groupTitleNotLogin == 'superadmin')
            {
                return abort(401);
            }
            elseif ($groupTitleLogin == 'admin' && ($groupTitleNotLogin == 'admin' || $groupTitleNotLogin == 'superadmin'))
            {
                return abort(401);
            }
            else {
                if (!empty($user)) {
                    $request->status ? $user->status = 1 : $user->status = 0;
                    $user->group_id = $request->group_id;

                    $user->save();
                    return redirect()->route('userEdit', ['id' => $user->id]);
                } else {
                    return abort(404);
                }
            }
        }
        else
        {
            return abort(401);
        }
    }

    /**
     * Permission
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function permission()
    {
        $permission = $this->checkPermission('permission');

        if ($permission)
        {

            $actions = Action::orderBy('id')->select('id', 'action_title')->get();
            $groups = Group::orderBy('id')->select('id', 'group_title')->get()->toArray();
            $groupAr = array();

            foreach ($groups as $key => $value)
            {
                $groupAr[$value['id']] = $value['group_title'];
            }

            return view('pages.users.permission', ['actions' => $actions, 'groupAr' => $groupAr]);
        }
        else
        {
            return abort(401);
        }
    }

    public function permissionUpdate(Request $request){
        $permission = $this->checkPermission('permission');

        if ($permission)
        {
            $groupId = $request->group_id;
            $actions = Action::orderBy('id')->select('id', 'action_title')->get();

            $groupsAction = GroupsAction::where('group_id', $groupId)->get();
            foreach ($groupsAction as $groupAction)
            {
                $x = GroupsAction::find($groupAction->id);
                $x->destroy($x->id);
            }

            foreach ($actions as $action)
            {
                $x = $action->action_title;
                if ($request->$x)
                {
                    $groupsAction = new GroupsAction;
                    $groupsAction->group_id = $groupId;
                    $groupsAction->action_id = $action->id;
                    $groupsAction->save();
                }
            }
            return redirect()->route('permission');
        }
        else
        {
            return abort(401);
        }
    }


    public function changePassword(Request $request)
    {
        if ($request->session()->has('errorPassword'))
        {
            return view('pages.users.changePassword', ['errorPassword'=>$request->session()->get('errorPassword')]);
        }
        else
        {
            return view('pages.users.changePassword');
        }

    }

    public function updatePassword(Request $request)
    {
        $this->validate($request, [
            'oldPassword' => 'required',
            'newPassword' => 'required'
        ]);

        if (Hash::check($request->oldPassword, Auth::user()->password)) {
            $request->user()->fill([
                'password' => Hash::make($request->newPassword)
            ])->save();

            return redirect()->route('home');
        }
        else
        {
            $request->session()->flash('errorPassword', 'Mật khẩu cũ không chính xác.');
            return redirect()->route('changePassword');

        }
    }

    public function checkAuth()
    {
        if (Auth::check())
        {
            $object = array();
            $object['id'] = Auth::id();
            $object['user_name'] = Auth::user()->name;
            $object['user_email'] = Auth::user()->email;
            $object['user_status'] = Auth::user()->status;
            $object['user_avatar'] = Auth::user()->avatar;
            $object['user_group'] = Auth::user()->group->group_title;

            return(json_encode($object)) ;
        }
        else
        {
            return null;
        }
    }
}
