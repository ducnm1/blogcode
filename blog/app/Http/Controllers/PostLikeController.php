<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\PostLike;
use Illuminate\Support\Facades\Auth;

class PostLikeController extends Controller
{
    public function apiLike($idPost, $idUser)
    {
        $post = Post::find($idPost);
        if (!empty($post))
        {
            $postLike = PostLike::where('post_id', $idPost)->where('user_id', $idUser)->first();

            if (empty($postLike))
            {
                $postLike = new PostLike;
                $postLike->user_id = $idUser;
                $postLike->post_id = $idPost;

                $postLike->save();

                $post->post_like++;

                $post->save();
            }
            else
            {
                $postLike->destroy($postLike->id);

                $post->post_like--;

                $post->save();
            }
        }
        else
        {
            // post undifined
        }
    }

    public function apiCheckStatusLiked($idPost, $idUser)
    {
        $postLike = PostLike::where('post_id', $idPost)->where('user_id', $idUser)->first();

        if (empty($postLike))
        {
            return 0;
        }
        else
        {
            return 1;
        }
    }
}
