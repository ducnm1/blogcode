<?php

namespace App\Http\Controllers;

use App\Company;
use App\Job;
use App\FollowCompany;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\EventListener\ValidateRequestListener;


class CompanyController extends Controller
{
    use shareMethod;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $company = new Company;
        $companies = $company->getList(24, 100);
    }

    public function management()
    {
        $company = new Company;
        $companies = $company->getList(24, 100, Auth::user());

        return view('pages.companies.management', ['companies'=> $companies]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.companies.create-edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'company_title' => 'required|max:255',
        ]);

        $company = new Company;

        $company->user_id = Auth::id();
        $company->company_title = $request->company_title;
        $company->company_title_clean = $this->EnToVn($company->company_title);
        $company->company_overview = $request->company_title;
        $company->company_address = $request->company_title;
        $company->company_website = $request->company_title;
        $company->company_email = $request->company_title;
        $company->company_size = $request->company_title;
        if ($request->hasFile('company_thumbnail') && $request->file('company_thumbnail')->isValid()) {
            $company->company_thumbnail = $this->uploadImage($request, 'company_thumbnail');
        }
        else
        {
            $company->company_thumbnail = '';
        }

        $company->save();

        return redirect()->route('companiesManagement');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($titleClean, $id)
    {
        $id = explode('-', $id);
        $idCompany = $id[count($id) - 1];
        unset($id[count($id) - 1]);
        $id = implode('-', $id);
        $titleClean = $titleClean . '-' . $id;

        $company = Company::find($idCompany);

        $jobs = $company->currentJobs($idCompany);

        if (Auth::check())
        {
            $follow = FollowCompany::where('user_id', Auth::id())
                                    ->where('company_id', $idCompany)->get();

            $statusFollow=count($follow);
            return view('pages.companies.index', ['company'=>$company, 'jobs'=>$jobs, 'statusFollow'=>$statusFollow]);
        }

        return view('pages.companies.index', ['company'=>$company, 'jobs'=>$jobs]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::find($id);

        if ($company && $company->user_id == Auth::id())
        {
            return view('pages.companies.create-edit', ['company'=>$company]);
        }
        else
        {
            abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $company = Company::find($id);

        if ($company && $company->user_id == Auth::id())
        {
            $this->validate($request, [
                'company_title' => 'required|max:255',
            ]);

            $company->user_id = Auth::id();
            $company->company_title = $request->company_title;
            $company->company_title_clean = $this->EnToVn($company->company_title);
            $company->company_overview = $request->company_title;
            $company->company_address = $request->company_title;
            $company->company_website = $request->company_title;
            $company->company_email = $request->company_title;
            $company->company_size = $request->company_title;
            if ($request->hasFile('company_thumbnail') && $request->file('company_thumbnail')->isValid()) {
                $company->company_thumbnail = $this->uploadImage($request, 'company_thumbnail');
            }
            else
            {
                $company->company_thumbnail = '';
            }

            $company->save();

            return redirect()->route('companiesManagement');
        }
        else
        {
            abort(404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $company = Company::find($id);
        if (!empty($company) && $company->user_id == Auth::id())
        {
            $company->destroy($id);
            return redirect()->route('companiesManagement');
        }
        else
        {
            return abort(404);
        }
    }
}
