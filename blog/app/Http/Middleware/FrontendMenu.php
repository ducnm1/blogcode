<?php

namespace App\Http\Middleware;

use Closure;
use Menu;

class FrontendMenu
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Menu::make('MyNavBar', function($menu){

            $menu->add('Home')->active();
            $menu->add('Html', array('route'  => array('postsOfCategory', 'category' => 'html')));
            $menu->add('Css', array('route'  => array('postsOfCategory', 'category' => 'css')));
            $menu->add('Javascript', array('route'  => array('postsOfCategory', 'category' => 'javascript')));

        });
        return $next($request);
    }
}
