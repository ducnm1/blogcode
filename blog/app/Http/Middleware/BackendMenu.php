<?php

namespace App\Http\Middleware;

use Closure;
use Menu;

class BackendMenu
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Menu::make('BackendMenu', function($menu){
            $dashboard = $menu->add('Dashboard', array('route'  => array('dashboard')));
            $menu->dashboard->append('</span>')
                            ->prepend('<i class="icon-home"></i><span class="title">');

            $menu->add('Quản lí bài viết', array('route'  => array('postsManagement')));
            $menu->add('Tạo bài viết', array('route'  => array('postCreat')));
            $menu->add('Quản lí chủ đề', array('route'  => array('categoriesManagement')));
            $menu->add('Tạo chủ đề', array('route'  => array('categoryCreat')));
            $menu->add('Phân quyền', array('route'  => array('permission')));
            $menu->add('Quản lí user', array('route'  => array('usersManagement')));
            $menu->add('Setting', array('route'  => array('optionEdit')));
            $menu->add('Đổi password', array('route'  => array('changePassword')));

            //Cv
            $menu->add('Tạo cv', array('route'=>'cvCreat'));
            $menu->add('Quản lí cv', array('route'=>'cvsManagement'));

            //Job
            $menu->add('Tạo việc làm', array('route'=>'jobCreat'));
            $menu->add('Quản lí việc làm', array('route'=>'jobsManagement'));

            //Job
            $menu->add('Tạo công ty', array('route'=>'companyCreat'));
            $menu->add('Quản lí công ty', array('route'=>'companiesManagement'));
        });
        return $next($request);
    }
}
