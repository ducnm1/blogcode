<?php
/**
 * Created by PhpStorm.
 * User: ducnm
 * Date: 10/18/2016
 * Time: 9:50 AM
 */

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use Route;

use Illuminate\Support\Facades\Auth;

use App\Action;

use App\Group;

use App\GroupsAction;


class ShareDataAllViewComposer
{

    protected $currentRouteName;


    public function __construct()
    {
        // Dependencies automatically resolved by service container...
        $this->currentRouteName = Route::currentRouteName();

        // Permission
        $this->myAuth = null;
        if (Auth::check())
        {
            $this->myAuth = Auth::user();
            $groupId = Auth::user()->group_id ;
            $actionsId = GroupsAction::where('group_id', $groupId)->select('action_id')->get()->toArray();
            $x = array();
            for ($i = 0; $i < count($actionsId); $i++) {
                array_push($x,$actionsId[$i]['action_id']);
            }
            $actionsTitle = Action::whereIn('id', $x)->select('action_title')->get()->toArray();

            $y = array();
            for ($i = 0; $i < count($actionsTitle); $i++) {
                array_push($y,$actionsTitle[$i]['action_title']);
            }
            $this->myAuth->permission = $y;
            $this->myAuth->group_title = Auth::user()->group->group_title;
        }
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with(['currentRouteName'=> $this->currentRouteName, 'myAuth'=> $this->myAuth]);
    }
}
