/**
 * Created by duc91bk on 22-Apr-17.
 */
var webpack = require('webpack');
var path = require('path');
//Thư mục sẽ chứa tập tin được biên dịch
var BUILD_DIR = path.resolve(__dirname, 'public/frontend/js');
//Thư mục chứa dự án - các component React
var APP_DIR = path.resolve(__dirname, 'resources/assets');

var config = {
    entry: {
        post_detail: APP_DIR + '/post/detail.jsx',
        post_list: APP_DIR + '/postList.jsx',
        test: APP_DIR + '/test.jsx',
    },
    output: {
        path: BUILD_DIR,
        filename: '[name].js'
    },
    module : {
        loaders : [
            {
                test : /\.jsx?/,
                include : APP_DIR,
                loader : 'babel-loader'
            }
        ]
    }
};

module.exports = config;