<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('company_id');
            $table->integer('career_id');
            $table->string('job_title', 200);
            $table->string('job_title_clean', 200);
            $table->string('job_salary', 50);
            $table->string('job_skill', 250);
            $table->string('job_address', 250);
            $table->mediumText('job_content');
            $table->mediumText('job_experience');
            $table->mediumText('job_benefit');
            $table->date('job_expired');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
