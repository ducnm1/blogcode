<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('comment_parent_id');
            $table->integer('comment_level')->default(1);
            $table->integer('post_id');
            $table->integer('user_id');
            $table->text('comment_content');
            $table->integer('comment_like')->default(0);
            $table->integer('comment_dislike')->default(0);
            $table->integer('comment_spam')->default(0);
            $table->boolean('comment_status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
