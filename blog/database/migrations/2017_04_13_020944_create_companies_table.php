<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('company_title', 255)->unique();
            $table->string('company_title_clean', 255)->unique();
            $table->string('company_thumbnail', 255);
            $table->string('company_address', 250);
            $table->string('company_website', 250);
            $table->string('company_email', 255);
            $table->string('company_size', 50);
            $table->mediumText('company_overview');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
