<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCvsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cvs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('career_id');
            $table->string('cv_title', 255);
            $table->string('cv_title_clean', 255);
            $table->string('cv_thumbnail', 255);
            $table->string('cv_address', 255);
            $table->string('cv_phone', 30);
            $table->string('cv_email', 255);
            $table->string('cv_skype', 255);
            $table->text('cv_personal_summary');
            $table->text('cv_work_experience');
            $table->text('cv_skill');
            $table->text('cv_academic');
            $table->boolean('cv_status_public')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cvs');
    }
}
