<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('options', function (Blueprint $table) {
            $table->increments('id');
            $table->string('option_person_contact');
            $table->text('option_description');
            $table->string('option_skype');
            $table->string('option_yahoo');
            $table->string('option_mail');
            $table->string('option_facebook');
            $table->string('option_twitter');
            $table->string('option_youtube');
            $table->string('option_address_1');
            $table->string('option_address_2');
            $table->string('option_address_3');
            $table->string('option_mobile_phone');
            $table->string('option_home_phone');
            $table->string('option_map_latitude');
            $table->string('option_map_longitude');
            $table->string('option_logo_big');
            $table->string('option_logo_small');
            $table->string('option_favicon');
            $table->boolean('option_pager')->default(1);
            $table->boolean('option_breadcrumb')->default(1);
            $table->boolean('option_viewer')->default(1);
            $table->boolean('option_like')->default(1);
            $table->boolean('option_dislike')->default(1);
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('options');
    }
}
