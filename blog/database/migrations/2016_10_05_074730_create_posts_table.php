<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('category_id');
            $table->string('post_title', 255)->unique();
            $table->string('post_title_clean', 255)->unique();
            $table->string('post_thumbnail', 255);
            $table->longText('post_content');
            $table->boolean('post_status')->default(1);
            $table->boolean('post_comment_status')->default(1);
            $table->integer('post_timer');
            $table->integer('post_view')->default(0);
            $table->integer('post_like')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
