<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $categories = ['html', 'css', 'javascript', 'jquery', 'angularjs', 'reactjs', 'php', 'laravel'];
        for ($i=0; $i < 8; $i++) {
            $item = new Category;
            $item->user_id = 1;
            //$item->category_parent_id = $faker->numberBetween($min = 1, $max = 5);
            $item->category_parent_id = 0;
            $item->category_level = $faker->numberBetween($min = 1, $max = 3);
            $item->category_title = $categories[$i];
            $item->category_title_clean = $item->category_title;
            $item->category_description = '';
            $item->category_thumbnail = '';

            $item->save();
        }
    }
}
