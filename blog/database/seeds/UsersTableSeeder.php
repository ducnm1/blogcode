<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i=0; $i <= 20; $i++) {
            $item = new User;
            if ($i == 20)
            {
                $item->group_id = 4;
                $item->name = 'test';
                $item->email = 'test@test.com';
                $item->avatar = $faker->imageUrl($width = 250, $height = 250);
                $item->password = bcrypt(123456);
            }
            else
            {
                $item->group_id = $faker->numberBetween($min = 1, $max = 4);
                $item->name = $faker->text($maxNbChars = 15).$i;
                $item->email = $faker->email;
                $item->avatar = $faker->imageUrl($width = 80, $height = 80);
                $item->password = bcrypt(123456);
            }
            $item->save();
        }
    }
}