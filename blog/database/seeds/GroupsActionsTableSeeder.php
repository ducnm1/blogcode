<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\GroupsAction;

class GroupsActionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $groups = array('member', 'author', 'admin', 'superadmin');
        $actions = array(
            'create_post',
            'edit_post',
            'delete_post',
            'create_category',
            'edit_category',
            'delete_category',
            'edit_user',
            'delete_user',
            'permission',
            'option'
        );

        // author
        for ($i=0; $i < 3; $i++) {
            $item = new GroupsAction;
            $item->group_id = 2;
            $item->action_id = $i + 1;
            $item->save();
        }

        //admin
        for ($i=0; $i < 6; $i++) {
            $item = new GroupsAction;
            $item->group_id = 3;
            $item->action_id = $i + 1;
            $item->save();
        }

        //superadmin
        for ($i=0; $i < 10; $i++) {
            $item = new GroupsAction;
            $item->group_id = 4;
            $item->action_id = $i + 1;
            $item->save();
        }
    }
}
