<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Option;

class OptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $item = new Option;

        $item->option_person_contact = $faker->name();
        $item->option_description = $faker->text(250);
        $item->option_skype = 'tieulehuyet91';
        $item->option_yahoo = 'tieulehuyet91';
        $item->option_mail = 'tieulehuyet91@gmail.com';
        $item->option_facebook = 'https://www.facebook.com/Nguyenminhducbk';
        $item->option_twitter = 'tieulehuyet91@gmail.com';
        $item->option_youtube = 'https://www.youtube.com/channel/UC9VbY1nPhWbAkBuEaAy0CNQ';
        $item->option_address_1 = $faker->address;
        $item->option_address_2 = $faker->address;
        $item->option_address_3 = $faker->address;
        $item->option_mobile_phone = $faker->phoneNumber;
        $item->option_home_phone = $faker->phoneNumber;
        $item->option_map_latitude = $faker->latitude;
        $item->option_map_longitude = $faker->longitude;
        $item->option_logo_big = $faker->imageUrl(200,150);
        $item->option_logo_small = $faker->imageUrl(200,150);
        $item->option_favicon = $faker->imageUrl(200,150);

        $item->save();

    }
}
