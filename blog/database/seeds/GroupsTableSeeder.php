<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Group;

class GroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $groups = array('member', 'author', 'admin', 'superadmin');

        for ($i=0; $i < count($groups); $i++) {
            $item = new Group;
            $item->group_title = $groups[$i];
            $item->save();
        }
    }
}
