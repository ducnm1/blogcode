<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ActionsTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(CommentsTableSeeder::class);
        $this->call(GroupsActionsTableSeeder::class);
        $this->call(GroupsTableSeeder::class);
        $this->call(PostsTableSeeder::class);
        $this->call(TagsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(OptionsTableSeeder::class);
        $this->call(CompaniesTableSeeder::class);
        $this->call(CvsTableSeeder::class);
        $this->call(JobsTableSeeder::class);
        $this->call(CareersTableSeeder::class);
        $this->call(SkillsTableSeeder::class);
    }
}
