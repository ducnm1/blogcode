<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Action;

class ActionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $actions = array(
            'create_post',
            'edit_post',
            'delete_post',
            'create_category',
            'edit_category',
            'delete_category',
            'edit_user',
            'delete_user',
            'permission',
            'option'
        );

        for ($i=0; $i < count($actions); $i++) {
            $item = new Action;
            $item->action_title = $actions[$i];
            $item->save();
        }
    }
}
