<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Cv;
use Carbon\Carbon;

class CvsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i=0; $i < 30; $i++) {
            $item = new Cv;
            $item->user_id = 21;
            $item->career_id = $faker->numberBetween($min = 1, $max = 10);
            $item->cv_title = $faker->text($maxNbChars = 50);
            $item->cv_title_clean = $this->EnToVn($item->cv_title);
            $item->cv_thumbnail = $faker->imageUrl($width = 200, $height = 200);
            $item->cv_address = $faker->address;
            $item->cv_phone = $faker->phoneNumber;
            $item->cv_email = $faker->email;
            $item->cv_skype = $faker->email;
            $item->cv_personal_summary = $faker->text($maxNbChars = 500);
            $item->cv_work_experience = $faker->text($maxNbChars = 500);
            $item->cv_skill = $faker->text($maxNbChars = 50);
            $item->cv_academic = $faker->text($maxNbChars = 200);

            $item->save();

        }
    }

    public function EnToVn($str)
    {
        if(!$str) return false;
        $unicode = array(
            'a'=>array('á','à','ả','ã','ạ','ă','ắ','ặ','ằ','ẳ','ẵ','â','ấ','ầ','ẩ','ẫ','ậ'),
            'A'=>array('Á','À','Ả','Ã','Ạ','Ă','Ắ','Ặ','Ằ','Ẳ','Ẵ','Â','Ấ','Ầ','Ẩ','Ẫ','Ậ'),
            'd'=>array('đ'),
            'D'=>array('Đ'),
            'e'=>array('é','è','ẻ','ẽ','ẹ','ê','ế','ề','ể','ễ','ệ'),
            'E'=>array('É','È','Ẻ','Ẽ','Ẹ','Ê','Ế','Ề','Ể','Ễ','Ệ'),
            'i'=>array('í','ì','ỉ','ĩ','ị'),
            'I'=>array('Í','Ì','Ỉ','Ĩ','Ị'),
            'o'=>array('ó','ò','ỏ','õ','ọ','ô','ố','ồ','ổ','ỗ','ộ','ơ','ớ','ờ','ở','ỡ','ợ'),
            'O'=>array('Ó','Ò','Ỏ','Õ','Ọ','Ô','Ố','Ồ','Ổ','Ỗ','Ộ','Ơ','Ớ','Ờ','Ở','Ỡ','Ợ'),
            'u'=>array('ú','ù','ủ','ũ','ụ','ư','ứ','ừ','ử','ữ','ự'),
            'U'=>array('Ú','Ù','Ủ','Ũ','Ụ','Ư','Ứ','Ừ','Ử','Ữ','Ự'),
            'y'=>array('ý','ỳ','ỷ','ỹ','ỵ'),
            'Y'=>array('Ý','Ỳ','Ỷ','Ỹ','Ỵ'),
            '-'=>array(' ','&quot;','.','-–-')
        );
        foreach($unicode as $nonUnicode=>$uni){
            foreach($uni as $value)
                $str = @str_replace($value,$nonUnicode,$str);
            $str = preg_replace("/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'| |\"|\&|\#|\[|\]|~|$|_/","-",$str);
            $str = preg_replace("/-+-/","-",$str);
            $str = preg_replace("/^\-+|\-+$/","",$str);
        }
        return strtolower($str);
    }
}
