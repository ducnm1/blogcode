<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Comment;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i=0; $i < 50; $i++) {
            $item = new Comment;
            $item->comment_parent_id = $faker->numberBetween($min = 1, $max = 20);
            $item->comment_level = $faker->numberBetween($min = 1, $max = 5);
            $item->post_id = $faker->numberBetween($min = 1, $max = 20);
            $item->user_id = $faker->numberBetween($min = 1, $max = 20);
            $item->comment_content = $faker->text($maxNbChars = 100);
            $item->comment_like = $faker->numberBetween($min = 1, $max = 20);
            $item->comment_dislike = $faker->numberBetween($min = 1, $max = 20);
            $item->comment_spam = $faker->numberBetween($min = 1, $max = 20);
            $item->comment_status = 1;

            $item->save();
        }
    }
}
