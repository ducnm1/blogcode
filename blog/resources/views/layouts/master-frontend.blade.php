<?php
/**
 * Created by PhpStorm.
 * User: duc91bk
 * Date: 27-Oct-16
 * Time: 9:08 PM
 */
?>
<!doctype html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    <!-- Styles -->
    <link href="/css/main.css" rel="stylesheet">
    <link href="/libraries/prism/prism.css" rel="stylesheet" />

</head>
<body class="@yield('body_class')">
    @include('partials.header')
    <div id="main-content" class="container">
    @section('main')
        This is main
    @show
    </div>
    @include('partials.footer')
    <!-- Scripts -->
    <script src="/js/libs.js"></script>
    <script src="/libraries/prism/prism.js"></script>
    <script src="/js/main.js"></script>
    @section('inject_script')
    @show


</body>
</html>

