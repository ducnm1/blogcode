<?php
/**
 * Created by PhpStorm.
 * User: ducnm
 * Date: 10/6/2016
 * Time: 9:21 AM
 */
?>
<header id="page-header" class="clearfix">
    @include('components.menu')

    <div class="box-search">
        {!! Form::open([
                'route' => 'search',
                'method' => 'GET'
            ]) !!}
        <div class="input-group">
            <div class="input-group-btn" id="search-in">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="value">Blog</span> <span class="caret"></span></button>
                <ul class="dropdown-menu" >
                    <li><a href="javascript:;">Blog</a></li>
                    <li><a href="javascript:;">Cv</a></li>
                    <li><a href="javascript:;">Job</a></li>
                    <li><a href="javascript:;">Company</a></li>
                    {{--<li role="separator" class="divider"></li>
                    <li><a href="javascript:;">All</a></li>--}}
                </ul>
            </div><!-- /btn-group -->
            <input type="hidden" name="searchIn" class="search-in" value="Blog">
            <input type="text" class="form-control" placeholder="Search for..." name="searchKey">
            <span class="input-group-btn">
                <span class="btn btn-default btn-submit" type="submit"><i class="fa fa-search"></i></span>
            </span>
        </div><!-- /input-group -->
        {!! Form::close() !!}
    </div>

</header>
{{--
<div class="clearfix">
    <div class="input-group">
        <div class="input-group-btn" id="search-in">
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="value">Blog</span> <span class="caret"></span></button>
            <ul class="dropdown-menu" >
                <li><a href="javascript:;">Blog</a></li>
                <li><a href="javascript:;">Cv</a></li>
                <li><a href="javascript:;">Job</a></li>
                <li><a href="javascript:;">Company</a></li>
            </ul>
        </div><!-- /btn-group -->
        <div class="input-group-btn" id="search-in">
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="value">Category</span> <span class="caret"></span></button>
            <ul class="dropdown-menu" >
                <li><a href="javascript:;">Blog</a></li>
                <li><a href="javascript:;">Cv</a></li>
                <li><a href="javascript:;">Job</a></li>
                <li><a href="javascript:;">Company</a></li>
            </ul>
        </div><!-- /btn-group -->
        <input type="text" class="form-control" placeholder="Search for..." name="searchKey">
        <span class="input-group-btn">
                <span class="btn btn-default btn-submit" type="submit"><i class="fa fa-search"></i></span>
            </span>
    </div><!-- /input-group -->
</div>

<div class="clearfix">
    <div class="input-group">
        <div class="input-group-btn" id="search-in">
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="value">Job</span> <span class="caret"></span></button>
            <ul class="dropdown-menu" >
                <li><a href="javascript:;">Blog</a></li>
                <li><a href="javascript:;">Cv</a></li>
                <li><a href="javascript:;">Job</a></li>
                <li><a href="javascript:;">Company</a></li>
            </ul>
        </div><!-- /btn-group -->
        <div class="input-group-btn" id="search-in">
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="value">Category</span> <span class="caret"></span></button>
            <ul class="dropdown-menu" >
                <li><a href="javascript:;">Blog</a></li>
                <li><a href="javascript:;">Cv</a></li>
                <li><a href="javascript:;">Job</a></li>
                <li><a href="javascript:;">Company</a></li>
            </ul>
        </div><!-- /btn-group -->
        <div class="input-group-btn" id="search-in">
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="value">Location</span> <span class="caret"></span></button>
            <ul class="dropdown-menu" >
                <li><a href="javascript:;">Blog</a></li>
                <li><a href="javascript:;">Cv</a></li>
                <li><a href="javascript:;">Job</a></li>
                <li><a href="javascript:;">Company</a></li>
            </ul>
        </div><!-- /btn-group -->
        <input type="text" class="form-control" placeholder="Search for..." name="searchKey">
        <span class="input-group-btn">
                <span class="btn btn-default btn-submit" type="submit"><i class="fa fa-search"></i></span>
            </span>
    </div><!-- /input-group -->
</div>--}}
