<?php
/**
 * Created by PhpStorm.
 * User: ducnm
 * Date: 10/6/2016
 * Time: 10:37 AM
 */
?>
@extends('layouts.master-frontend')
@section('body_class', 'home-page')
@section('main')
    <h3><a href="{{ route('postsOfCategory') }}">Blog</a></h3>
    <div class="list-item row">
        @foreach ($posts as $post)
        <div class="col-xs-6 col-sm-4">
            @include('pages.posts.partials.list-item')
        </div>
        @endforeach
    </div>

    <h3><a href="{{ route('jobList') }}">Job</a></h3>
    <div class="list-item row">
        @foreach ($jobs as $job)
            <div class="col-xs-6 col-sm-4">
                @include('pages.jobs.partials.list-item')
            </div>
        @endforeach
    </div>

    <h3><a href="{{ route('companyList') }}">Company</a></h3>
    <div class="list-item row">
        @foreach ($companies as $company)
            <div class="col-xs-6 col-sm-4">
                @include('pages.companies.partials.list-item')
            </div>
        @endforeach
    </div>

@endsection
