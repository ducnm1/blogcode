<?php
/**
 * Created by PhpStorm.
 * User: duc91bk
 * Date: 16-Apr-17
 * Time: 10:46 AM
 */
?>
<div class="item">
    <a href="{{ route('cvDetail', ['titleClean'=>$cv->cv_title_clean, 'id'=>$cv->id]) }}">
        <img src="{{ $cv->cv_thumbnail }}" alt="{{ $cv->cv_title }}">
    </a>
    <h3 class="item-title"><a href="{{ route('cvDetail', ['titleClean'=>$cv->cv_title_clean, 'id'=>$cv->id]) }}">{{ $cv->cv_title }}</a></h3>
    <p class="item-description">{{ $cv->career_title  }}</p>
    <p class="item-description">{{ $cv->cv_skill }}</p>
</div>
