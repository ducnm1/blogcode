<?php
/**
 * Created by PhpStorm.
 * User: duc91bk
 * Date: 16-Apr-17
 * Time: 3:49 PM
 */
?>
@extends('layouts.master-frontend')
@section('body_class', 'blog-page')
@section('main')
    <div class="entry-content">
        <img src="{{ $cv->cv_thumbnail }}" alt="{{ $cv->career_title }}">
        <h3>{{$cv->cv_title}}</h3>
        <p>Career: {{ $cv->career_title }}</p>

        <p>Phone: {{ $cv->cv_phone }}</p>
        <p>Email: {{ $cv->cv_email }}</p>
        <p>Phone: {{ $cv->cv_phone }}</p>
        <p>Address: {{ $cv->cv_address }}</p>
        <p>Skill: {{ $cv->cv_skill }}</p>
        <div class="person-summary">
            <h3>Person summary</h3>
            <div>{{ $cv->cv_personal_summary }}</div>
        </div>

        <div class="work-experience">
            <h3>Work expericence</h3>
            <div>
                {{ $cv->cv_work_experience }}
            </div>
        </div>

        <div class="academic">
            <h3>Academic</h3>
            <div>
                {{ $cv->cv_academic }}
            </div>
        </div>
    </div>
@endsection
