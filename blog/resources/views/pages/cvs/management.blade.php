<?php
/**
 * Created by PhpStorm.
 * User: duc91bk
 * Date: 25-Oct-16
 * Time: 11:17 PM
 */
?>
@extends('layouts.master-backend')

@section('main')
    @if(count($cvs))
    <table class="table table-responsive table-striped table-bordered">
        <thead>
            <tr>
                <th>#</th>
                <th>Tiêu đề</th>
                <th>Trạng thái</th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        @foreach ($cvs as $cv)
            <tr>
                <td>{{ $loop->index }}</td>
                <td><a href="{{ route('cvDetail', ['titleClean'=> $cv->cv_title_clean, 'id'=>$cv->id]) }}">{{ $cv->cv_title }}</a></td>

                <td>
                    @if($cv->cv_status_public)
                        Công khai
                    @else
                        Không công khai
                    @endif
                </td>

                <td><a href="{{ route('cvEdit', ['id' => $cv->id]) }}">Sửa</a></td>

                <td><a href="{{ route('cvDestroy', ['id' => $cv->id]) }}">Xóa</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @else
        <p>Bạn chưa có cv nào.

            <a href="{{ route('cvCreat') }}" class="btn btn-primary">Tạo cv</a>
        </p>
    @endif
    {{ $cvs->links() }}
@endsection
