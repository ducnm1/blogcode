<?php
/**
 * Created by PhpStorm.
 * User: ducnm
 * Date: 10/12/2016
 * Time: 10:43 AM
 */
?>

@extends('layouts.master-backend')

@section('main')
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if($currentRouteName == 'cvCreat')
            {!! Form::open([
                'route' => 'cvStore',
                'method' => 'POST',
                'files'=>true
            ]) !!}
        @else
            {!! Form::model($cv, [
                'route' => ['cvUpdate', $cv->id],
                'method' => 'PUT',
                //'files'=>true
            ]) !!}
        @endif
            <div class="form-group">
                {!! Form::label('cv_title', 'Tiêu đề') !!}
                {!! Form::text('cv_title',null, ['class'=>'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('career_id', 'Nghề nghiệp') !!}
                {!! Form::select('career_id', $careers, null, ['class'=>'form-control']) !!}
            </div>
            {{--<div class="form-group">--}}
                {{--{!! Form::label('cv_thumbnail', 'Avatar') !!}--}}
                {{--{!! Form::text('cv_thumbnail', Auth::user()->avatar, ['class'=>'form-control']) !!}--}}
            {{--</div>--}}
            <div class="form-group">
                {!! Form::label('cv_address', 'Địa chỉ') !!}
                {!! Form::text('cv_address',null, ['class'=>'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('cv_phone', 'Số điện thoại') !!}
                {!! Form::text('cv_phone',null, ['class'=>'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('cv_email', 'Email') !!}
                {!! Form::text('cv_email',null, ['class'=>'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('cv_skype', 'Skype') !!}
                {!! Form::text('cv_skype',null, ['class'=>'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('cv_skill', 'Skill') !!}
                {!! Form::text('cv_skill',null, ['class'=>'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('cv_personal_summary', 'Tóm tắt cá nhân') !!}
                {!! Form::textarea('cv_personal_summary',null, ['class'=>'form-control content-tinymce']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('cv_work_experience', 'Kinh nghiệm làm việc') !!}
                {!! Form::textarea('cv_work_experience',null, ['class'=>'form-control content-tinymce']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('cv_academic', 'Giáo dục') !!}
                {!! Form::textarea('cv_academic',null, ['class'=>'form-control content-tinymce']) !!}
            </div>

            <div class="form-group">
                <label for="cv_status_public">
                    Public <span class="fa fa-question-circle" title="Nếu chọn thuộc tính này người dùng sẽ không thể tìm hấy cv của bạn qua công cụ search, chỉ có nơi bạn nộp cv mới xem được cv của bạn."></span>
                </label>

                {!! Form::checkbox('cv_status_public', null, true) !!}
            </div>

            <button type="submit" class="btn btn-success">Save</button>
        {!! Form::close() !!}

    <style>
        @media (min-width: 768px) {
            #upload-file-manager .modal-dialog {
                width: 1200px;
            }

            #upload-file-manager .modal-content {
                overflow: hidden;
            }

            #upload-file-manager .modal-body {
                padding: 0;
                background: #ececec;
            }
        }
        .upload-thumbnail-preview-image {
            margin-top: 20px;
        }
        .upload-thumbnail-preview-image img {
            width: 100%;
            height: auto;
            display: block;
        }
    </style>
    <script>
        /*$(document).ready(function () {
            previewThumbnail();
            $('#post_thumbnail').change(function () {
                previewThumbnail();
            });
            $('#upload-file-manager').on('hidden.bs.modal', function (e) {
                previewThumbnail();
            })

            function previewThumbnail() {
                if ($('#post_thumbnail').val())
                {
                    var $postThumbnail = $('#post_thumbnail').val();
                    var $domain = window.location.origin;
                    $postThumbnail = $postThumbnail.replace($domain, '');
                    var $pathThumbnail = $postThumbnail.split('/');
                    if ($pathThumbnail[1] == 'upload')
                    {
                        var $filename = $postThumbnail.replace(/^.*[\\\/]/, '');
                        $postThumbnail = "/thumbs/" + $filename;
                        $('#post_thumbnail').val($postThumbnail);
                    }
                    var $img = '<img src="'+ $postThumbnail +'" />';
                    $('.upload-thumbnail-preview-image').removeClass('hide').html($img);
                }
            }
        });*/


    </script>

    <script>
        /*$(document).ready(function () {
            upload_thumbnail ();
            function upload_thumbnail () {
                var myReader = new FileReader();
                $("input[type='file']").on('change', function () {
                    var $this = $(this);
                    if (typeof (FileReader) != "undefined") {
                        var image_holder = $this.closest('.wrap-thumb').find(".image-holder");
                        image_holder.empty();
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $("<img />", {
                                "src": e.target.result,
                                "class": "thumb-image"
                            }).appendTo(image_holder);
                        }
                        reader.readAsDataURL($(this)[0].files[0]);
                    } else {
                        alert("This browser does not support FileReader.");
                    }
                });
            }
        });*/

    </script>
    <style>
        .image-holder {
            margin-top: 10px;
            max-width: 400px;
        }
        .image-holder img {
            display: block;
            max-width: 100%;
            height: auto;
        }
    </style>
@endsection