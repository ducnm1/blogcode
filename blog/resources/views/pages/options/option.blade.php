<?php
/**
 * Created by PhpStorm.
 * User: ducnm
 * Date: 10/12/2016
 * Time: 10:43 AM
 */
?>

@extends('layouts.master-backend')

@section('main')
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    {!! Form::model($option, [
        'route' => ['optionUpdate', $option->id],
        'method' => 'PUT',
        'files' => true
    ]) !!}
        <div class="form-group">
            {!! Form::label('option_person_contact', 'Người liên hệ') !!}
            {!! Form::text('option_person_contact',null, ['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('option_description', 'Mô tả') !!}
            {!! Form::text('option_description',null, ['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('option_skype', 'Skype') !!}
            {!! Form::text('option_skype',null, ['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('option_yahoo', 'Yahoo') !!}
            {!! Form::text('option_yahoo',null, ['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('option_mail', 'Mail') !!}
            {!! Form::text('option_mail',null, ['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('option_facebook', 'Facebook') !!}
            {!! Form::text('option_facebook',null, ['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('option_twitter', 'Twitter') !!}
            {!! Form::text('option_twitter',null, ['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('option_youtube', 'Youtube') !!}
            {!! Form::text('option_youtube',null, ['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('option_address_1', 'Địa chỉ 1') !!}
            {!! Form::text('option_address_1',null, ['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('option_address_2', 'Địa chỉ 2') !!}
            {!! Form::text('option_address_2',null, ['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('option_address_3', 'Địa chỉ 3') !!}
            {!! Form::text('option_address_3',null, ['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('option_mobile_phone', 'Điện thoại di động') !!}
            {!! Form::text('option_mobile_phone',null, ['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('option_home_phone', 'Điện thoại bàn') !!}
            {!! Form::text('option_home_phone',null, ['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            <label for="option_map_latitude">
                Địa chỉ google map
                <a href="https://sites.google.com/site/caubedewetestup/test-bai/huongdanlaytoadotrengooglemap" class="fa fa-question-circle" title="Địa chỉ google map là gì?"></a>
            </label>
            <div class="form-inline">
                <div class="form-group">
                    {!! Form::text('option_map_latitude',null, ['class'=>'form-control', 'placeholder'=>'Latitude']) !!}
                </div>
                <div class="form-group">
                    {!! Form::text('option_map_longitude',null, ['class'=>'form-control', 'placeholder'=>'Longitude']) !!}
                </div>
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('option_logo_big', 'Logo chính') !!}
            <div class="wrap-thumb">
                {!! Form::file('option_logo_big') !!}
                <div class="image-holder">
                    @if(!empty($option->option_logo_big))
                        <img src="{{ $option->option_logo_big }}" alt="option_logo_big">
                    @endif
                </div>
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('option_logo_small', 'Logo phụ') !!}
            <div class="wrap-thumb">
                {!! Form::file('option_logo_small') !!}
                <div class="image-holder">
                    @if(!empty($option->option_logo_small))
                        <img src="{{ $option->option_logo_small }}" alt="option_logo_small">
                    @endif
                </div>
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('option_favicon', 'Logo favicon') !!}
            <div class="wrap-thumb">
                {!! Form::file('option_favicon') !!}
                <div class="image-holder">
                    @if(!empty($option->option_favicon))
                        <img src="{{ $option->option_favicon }}" alt="option_favicon">
                    @endif
                </div>
            </div>
        </div>

        <div class="form-group">
            {!! Form::checkbox('option_pager', null) !!}
            {!! Form::label('option_pager', 'Hiển thị pager') !!}
        </div>

        <div class="form-group">
            {!! Form::checkbox('option_viewer', null) !!}
            {!! Form::label('option_viewer', 'Hiển thị số người xem trang') !!}
        </div>

        <div class="form-group">
            {!! Form::checkbox('option_breadcrumb', null) !!}
            {!! Form::label('option_breadcrumb', 'Hiển thị breadcrumb') !!}
        </div>

        <div class="form-group">
            {!! Form::checkbox('option_like', null) !!}
            {!! Form::label('option_like', 'Hiển thị số người like') !!}
        </div>

        <div class="form-group">
            {!! Form::checkbox('option_like', null) !!}
            {!! Form::label('option_like', 'Hiển thị số người like') !!}
        </div>

        <button type="submit" class="btn btn-success">Save</button>
    {!! Form::close() !!}
    <script>
        upload_thumbnail ();
        function upload_thumbnail () {
            var myReader = new FileReader();
            $("input[type='file']").on('change', function () {
                var $this = $(this);
                if (typeof (FileReader) != "undefined") {
                    var image_holder = $this.closest('.wrap-thumb').find(".image-holder");
                    image_holder.empty();
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $("<img />", {
                            "src": e.target.result,
                            "class": "thumb-image"
                        }).appendTo(image_holder);
                    }
                    reader.readAsDataURL($(this)[0].files[0]);
                } else {
                    alert("This browser does not support FileReader.");
                }
            });
        }
    </script>
    <style>
        .image-holder {
            margin-top: 10px;
            max-width: 400px;
        }
        .image-holder img {
            display: block;
            max-width: 100%;
            height: auto;
        }
    </style>
@endsection