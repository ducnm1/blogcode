<?php
/**
 * Created by PhpStorm.
 * User: ducnm
 * Date: 10/12/2016
 * Time: 10:43 AM
 */
?>

@extends('layouts.master-backend')

@section('main')
    <div class="container">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if($currentRouteName == 'categoryCreat')
            {!! Form::open([
                'route' => 'categoryStore',
                'method' => 'POST',
                'files'=>true
            ]) !!}
        @else
            {!! Form::model($category, [
                'route' => ['categoryUpdate', $category->id],
                'method' => 'PUT',
                'files'=>true
            ]) !!}
        @endif
        <div class="form-group">
            {!! Form::label('category_title', 'Tiêu đề') !!}
            {!! Form::text('category_title',null, ['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('category_description', 'Miêu tả') !!}
            {!! Form::textarea('category_description',null, ['class'=>'form-control']) !!}
        </div>
        @if(count($categories))
        <div class="form-group">
            {!! Form::label('category_parent_id', 'Chủ đề cha') !!}
            {!! Form::select('category_parent_id', $categories, null) !!}
        </div>
        @endif

        <div class="row form-group">
            <div class="col-sm-4">
                <div class="upload-thumbnail">
                    <div class="input-group">
                        @if(empty($category->category_thumbnail))
                            <input type="text" id="post_thumbnail" name="category_thumbnail" class="form-control" placeholder="Ảnh đại diện">
                        @else
                            <input type="text" id="post_thumbnail" name="category_thumbnail" class="form-control" placeholder="Ảnh đại diện" value="{{ $category->category_thumbnail  }}">
                        @endif
                        <span class="input-group-btn">
                            <a data-toggle="modal" class="btn btn-default" href="javascript:;" data-target="#upload-file-manager">Select</a>
                        </span>
                    </div><!-- /input-group -->
                    <div class="upload-thumbnail-preview-image hide">

                    </div>
                </div>
            </div>
        </div>


        <button type="submit" class="btn btn-success">Save</button>
        {!! Form::close() !!}
    </div>
    <div class="modal fade" id="upload-file-manager" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Upload ảnh đại diện</h4>
                </div>
                <div class="modal-body">
                    <iframe width="100%" height="600px"
                            src="/filemanager/dialog.php?type=2&amp;field_id=post_thumbnail'&amp;fldr=" frameborder="0"
                            style="overflow: scroll; overflow-x: hidden; overflow-y: scroll; "></iframe>
                </div>

            </div>
        </div>
    </div>
    <style>
        @media (min-width: 768px) {
            #upload-file-manager .modal-dialog {
                width: 1200px;
            }

            #upload-file-manager .modal-content {
                overflow: hidden;
            }

            #upload-file-manager .modal-body {
                padding: 0;
                background: #ececec;
            }
        }
        .upload-thumbnail-preview-image {
            margin-top: 20px;
        }
        .upload-thumbnail-preview-image img {
            width: 100%;
            height: auto;
            display: block;
        }
    </style>
    <script>
        previewThumbnail();
        $('#post_thumbnail').change(function () {
            previewThumbnail();
        });
        $('#upload-file-manager').on('hidden.bs.modal', function (e) {
            previewThumbnail();
        })

        function previewThumbnail() {
            if ($('#post_thumbnail').val())
            {
                var $postThumbnail = $('#post_thumbnail').val();
                var $domain = window.location.origin;
                $postThumbnail = $postThumbnail.replace($domain, '');
                var $pathThumbnail = $postThumbnail.split('/');
                if ($pathThumbnail[1] == 'upload')
                {
                    var $filename = $postThumbnail.replace(/^.*[\\\/]/, '');
                    $postThumbnail = "/thumbs/" + $filename;
                    $('#post_thumbnail').val($postThumbnail);
                }
                var $img = '<img src="'+ $postThumbnail +'" />';
                $('.upload-thumbnail-preview-image').removeClass('hide').html($img);
            }
        }

    </script>
@endsection