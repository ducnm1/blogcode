<?php
/**
 * Created by PhpStorm.
 * User: duc91bk
 * Date: 25-Oct-16
 * Time: 11:17 PM
 */
?>
@extends('layouts.master-backend')

@section('main')
    <table class="table table-responsive table-striped table-bordered">
        <thead>
            <tr>
                <th>#</th>
                <th>Tiêu đề</th>
                <th>Tác giả</th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        @foreach ($categories as $category)
            <tr>
                <td>{{ $category->id }}</td>
                <td><a href="{{ route('postsOfCategory', ['categoryTitleClean'=> $category->category_title_clean]) }}">{{ $category->category_title }}</a></td>
                <td>{{ $category->name  }}</td>
                <td><a href="{{ route('categoryEdit', ['id' => $category->id]) }}">Sửa</a></td>
                <td><a href="{{ route('categoryDestroy', ['id' => $category->id]) }}">Xóa</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $categories->links() }}
@endsection
