<?php
/**
 * Created by PhpStorm.
 * User: ducnm
 * Date: 10/12/2016
 * Time: 10:43 AM
 */
?>

@extends('layouts.master-backend')

@section('main')
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if($currentRouteName == 'postCreat')
            {!! Form::open([
                'route' => 'postStore',
                'method' => 'POST',
                'files'=>true
            ]) !!}
        @else
            {!! Form::model($post, [
                'route' => ['postUpdate', $post->id],
                'method' => 'PUT',
                'files'=>true
            ]) !!}
        @endif
            <div class="form-group">
                {!! Form::label('post_title', 'Tiêu đề') !!}
                {!! Form::text('post_title',null, ['class'=>'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('post_content', 'Nội dung') !!}
                {!! Form::textarea('post_content',null, ['class'=>'form-control content-tinymce']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('category_id', 'Chủ đề') !!}
                {!! Form::select('category_id', $categories, null, ['class'=>'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('post_thumbnail', 'Ảnh đại diện') !!}
                <div class="wrap-thumb">
                    {!! Form::file('post_thumbnail') !!}
                    <div class="image-holder">
                        @if(!empty($post->post_thumbnail))
                            <img src="{{ $post->post_thumbnail }}" alt="post_thumbnail">
                        @endif
                    </div>
                </div>
            </div>
            {{--<div class="row form-group">--}}
                {{--<div class="col-sm-4">--}}
                    {{--<div class="upload-thumbnail">--}}
                        {{--<div class="input-group">--}}
                            {{--@if(empty($post->post_thumbnail))--}}
                                {{--<input type="text" id="post_thumbnail" name="post_thumbnail" class="form-control" placeholder="Ảnh đại diện">--}}
                            {{--@else--}}
                                {{--<input type="text" id="post_thumbnail" name="post_thumbnail" class="form-control" placeholder="Ảnh đại diện" value="{{ $post->post_thumbnail  }}">--}}
                            {{--@endif--}}
                            {{--<span class="input-group-btn">--}}
                                {{--<a data-toggle="modal" class="btn btn-default" href="javascript:;" data-target="#upload-file-manager">Select</a>--}}
                            {{--</span>--}}
                        {{--</div><!-- /input-group -->--}}
                        {{--<div class="upload-thumbnail-preview-image hide">--}}

                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            <div class="form-group">
                {!! Form::label('post_tags', 'Tags') !!}
                {!! Form::text('post_tags',null, ['class'=>'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('post_comment_status', 'Cho phép bình luận') !!}
                {!! Form::checkbox('post_comment_status', null, true) !!}
            </div>
            <div class="form-group">
                {!! Form::label('post_status', 'Trạng thái') !!}
                {!! Form::checkbox('post_status', null, true) !!}
            </div>

            <div class="form-group">
                {!! Form::label('post_timer', 'Hẹn giờ') !!}
                @if($currentRouteName == 'postCreat')
                    <input type="datetime" name="post_timer" class="form-control" value="{{\Carbon\Carbon::now()}}">
                @else
                    <input type="datetime" name="post_timer" class="form-control" value="{{ $post->post_timer }}">
                @endif
            </div>


            <button type="submit" class="btn btn-success">Save</button>
        {!! Form::close() !!}
    <div class="modal fade" id="upload-file-manager" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Upload ảnh đại diện</h4>
                </div>
                <div class="modal-body">
                    <iframe width="100%" height="600px"
                            src="/filemanager/dialog.php?type=2&amp;field_id=post_thumbnail'&amp;fldr=" frameborder="0"
                            style="overflow: scroll; overflow-x: hidden; overflow-y: scroll; "></iframe>
                </div>

            </div>
        </div>
    </div>
    <style>
        @media (min-width: 768px) {
            #upload-file-manager .modal-dialog {
                width: 1200px;
            }

            #upload-file-manager .modal-content {
                overflow: hidden;
            }

            #upload-file-manager .modal-body {
                padding: 0;
                background: #ececec;
            }
        }
        .upload-thumbnail-preview-image {
            margin-top: 20px;
        }
        .upload-thumbnail-preview-image img {
            width: 100%;
            height: auto;
            display: block;
        }
    </style>
    <script>
        previewThumbnail();
        $('#post_thumbnail').change(function () {
            previewThumbnail();
        });
        $('#upload-file-manager').on('hidden.bs.modal', function (e) {
            previewThumbnail();
        })

        function previewThumbnail() {
            if ($('#post_thumbnail').val())
            {
                var $postThumbnail = $('#post_thumbnail').val();
                var $domain = window.location.origin;
                $postThumbnail = $postThumbnail.replace($domain, '');
                var $pathThumbnail = $postThumbnail.split('/');
                if ($pathThumbnail[1] == 'upload')
                {
                    var $filename = $postThumbnail.replace(/^.*[\\\/]/, '');
                    $postThumbnail = "/thumbs/" + $filename;
                    $('#post_thumbnail').val($postThumbnail);
                }
                var $img = '<img src="'+ $postThumbnail +'" />';
                $('.upload-thumbnail-preview-image').removeClass('hide').html($img);
            }
        }

    </script>

    <script>
        upload_thumbnail ();
        function upload_thumbnail () {
            var myReader = new FileReader();
            $("input[type='file']").on('change', function () {
                var $this = $(this);
                if (typeof (FileReader) != "undefined") {
                    var image_holder = $this.closest('.wrap-thumb').find(".image-holder");
                    image_holder.empty();
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $("<img />", {
                            "src": e.target.result,
                            "class": "thumb-image"
                        }).appendTo(image_holder);
                    }
                    reader.readAsDataURL($(this)[0].files[0]);
                } else {
                    alert("This browser does not support FileReader.");
                }
            });
        }
    </script>
    <style>
        .image-holder {
            margin-top: 10px;
            max-width: 400px;
        }
        .image-holder img {
            display: block;
            max-width: 100%;
            height: auto;
        }
    </style>
@endsection