<?php
/**
 * Created by PhpStorm.
 * User: duc91bk
 * Date: 25-Oct-16
 * Time: 11:17 PM
 */
?>
@extends('layouts.master-backend')

@section('main')
    @if(count($posts))
    <table class="table table-responsive table-striped table-bordered">
        <thead>
            <tr>
                <th>#</th>
                <th>Tiêu đề</th>
                <th>Tác giả</th>
                <th>Chuyên mục</th>
                <th>Trạng thái</th>
                <th>Ngày xuất bản</th>
                @if(in_array("edit_post", $myAuth->permission))
                <th></th>
                @endif
                @if(in_array("delete_post", $myAuth->permission))
                <th></th>
                @endif
            </tr>
        </thead>
        <tbody>
        @foreach ($posts as $post)
            <tr>
                <td>{{ $loop->index }}</td>
                <td><a href="{{ route('postDetail', ['categoryTitleClean'=> $post->category_title_clean, 'titleClean'=>$post->post_title_clean, 'id'=>$post->id]) }}">{{ $post->post_title }}</a></td>
                <td>{{ $post->name  }}</td>
                <td>{{ $post->category_title  }}</td>
                <td>
                    @if($post->post_status)
                        Công khai
                    @else
                        Bản nháp
                    @endif
                </td>
                <td>{{ $post->post_timer }}</td>

                <td><a href="{{ route('postEdit', ['id' => $post->id]) }}">Sửa</a></td>

                <td><a href="{{ route('postDestroy', ['id' => $post->id]) }}">Xóa</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @else
        <p>Bạn chưa có bài viết nào.
            @if(in_array("create_post", $myAuth->permission))
            <a href="{{ route('postCreat') }}" class="btn btn-primary">Tạo bài viết</a>
            @endif
        </p>
    @endif
    {{ $posts->links() }}
@endsection
