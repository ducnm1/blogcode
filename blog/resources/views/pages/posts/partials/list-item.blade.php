<?php
/**
 * Created by PhpStorm.
 * User: duc91bk
 * Date: 16-Apr-17
 * Time: 10:44 AM
 */
?>
<div class="item">
    <a href="{{ route('postDetail', ['titleClean'=>$post->post_title_clean, 'id'=>$post->id]) }}">
        <img src="{{ $post->post_thumbnail }}" alt="{{ $post->post_title }}">
    </a>
    <h3 class="item-title"><a href="{{ route('postDetail', ['titleClean'=>$post->post_title_clean, 'id'=>$post->id]) }}">{{ $post->post_title }}</a></h3>
    <p class="item-description">{{ $post->post_content  }}</p>
</div>
