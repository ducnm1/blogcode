<?php
/**
 * Created by PhpStorm.
 * User: ducnm
 * Date: 11/2/2016
 * Time: 9:56 AM
 */
?>
<?php
/**
 * Created by PhpStorm.
 * User: ducnm
 * Date: 10/6/2016
 * Time: 10:37 AM
 */
?>
@extends('layouts.master-frontend')
@section('body_class', 'home-page')
@section('main')
    <div class="container">
        <div class="clearfix" class="searchfor">
            <span>Kết quả tìm kiếm cho: {{ $searchKey }}</span>
        </div>
        <div class="list-item row">
            @if($searchIn == 'blog')
                @foreach ($posts as $post)
                    <div class="col-xs-6 col-sm-4">
                        @include('pages.posts.partials.list-item')
                    </div>
                @endforeach
            @elseif($searchIn == 'job')
                @foreach ($jobs as $job)
                    <div class="col-xs-6 col-sm-4">
                        @include('pages.jobs.partials.list-item')
                    </div>
                @endforeach
            @elseif($searchIn == 'company')
                @foreach ($companies as $company)
                    <div class="col-xs-6 col-sm-4">
                        @include('pages.companies.partials.list-item')
                    </div>
                @endforeach
            @elseif($searchIn == 'cv')
                @foreach ($cvs as $cv)
                    <div class="col-xs-6 col-sm-4">
                        @include('pages.cvs.partials.list-item')
                    </div>
                @endforeach
            @endif
        </div>
        <div class="clearfix">
            <div class="pull-right">
                @if($searchIn == 'blog')
                    {{ $posts->links() }}
                @elseif($searchIn == 'job')
                    {{ $jobs->links() }}
                @elseif($searchIn == 'company')
                    {{ $companies->links() }}
                @elseif($searchIn == 'cv')
                    {{ $cvs->links() }}
                @endif
            </div>
        </div>
    </div>
@endsection

