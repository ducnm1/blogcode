<?php
/**
 * Created by PhpStorm.
 * User: ducnm
 * Date: 10/6/2016
 * Time: 4:07 PM
 */
?>
@extends('layouts.master-frontend')

@section('main')
    <div class="row">
        <aside id="sidebar-left" class="col-xs-12 col-md-3">
            <div class="widget widget-list">
                <h3 class="widget-title">Các bài liên quan</h3>
                <ul class="list-unstyled">
                    @foreach ($itemsRelated as $itemRelated)
                        <li>
                            <a href="{{ route('postDetail', ['titleClean'=>$itemRelated->post_title_clean, 'id'=>$itemRelated->id]) }}">{{ $itemRelated->post_title  }}</a>
                        </li>
                    @endforeach
                </ul>
            </div>

        </aside>
        <!-- sidebar left -->
        <section id="main-col" class="col-xs-12 col-sm-12 col-md-9">
            <ul class="list-inline">
                <li><a href="/">Home</a></li>
                @for ($i = 0; $i < count($breadcrumb); $i++)
                    <li>/ <a href="{{ route('postsOfCategory', ['categoryTitleClean'=> $breadcrumb[$i]['category_title_clean']]) }}">{{ $breadcrumb[$i]['category_title'] }}</a></li>
                @endfor
            </ul>
            <div class="entry-post">
                <h1 class="entry-title">{{ $item->post_title }}</h1>
                <div class="entry-metadata clearfix">
                    <ul class="list-unstyled">
                        <li><a href="{{ route('user', ['username'=> $item->user_name]) }}"><i class="fa fa-user"></i> {{ $item->user_name }}</a></li>

                        @if($item->post_view > 0)
                            <li><i class="fa fa-eye"></i> {{  $item->post_view }} views</li>
                        @endif

                        <li><i class="fa fa-clock-o"></i> {{ $item->post_timer }}</li>
                        <li>
                            <button class="btn" type="submit">
                                <i class="fa fa-thumbs-up"></i>{{ $item->post_like }} like
                            </button>
                        </li>
                    </ul>
                </div>
                <div class="entry-content">
                    {!! $item->post_content !!}
                </div>
                @if(count($item->tags))
                <div class="entry-metadata clearfix">
                    <i class="fa fa-tags"></i>
                    <?php $i = 0; ?>
                    @foreach($item->tags as $tag)
                        <?php $i++; ?>
                        @if(count($item->tags) == $i)
                            <a href="{{ route('postsOfTag', ['tagTitleClean'=>$tag->tag_title_clean]) }}">{{ $tag->tag_title }}</a>
                        @else
                            <a href="{{ route('postsOfTag', ['tagTitleClean'=>$tag->tag_title_clean]) }}">{{ $tag->tag_title }}</a>,
                        @endif
                    @endforeach
                </div>
                @endif
                <input type="hidden" id="post-id" value="{{$item->id}}">
            </div>
        </section>
        <!-- main col -->
    </div>
@endsection
