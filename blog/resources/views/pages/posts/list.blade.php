<?php
/**
 * Created by PhpStorm.
 * User: duc91bk
 * Date: 25-Oct-16
 * Time: 11:17 PM
 */
?>
@extends('layouts.master-frontend')
@section('body_class', 'blog-page')
@section('main')
    @if(!empty($posts))
        {!! Form::open([
                'route' => 'postsOfCategory',
                'method' => 'GET',
                'id'=>'list-filter'
            ]) !!}
        <div class="clearfix">
            <div class="pull-left">
                Sort
                <select name="sort" class="form-control" style="display: inline-block; width: auto;">
                    @if($sort == 'time')
                        <option value="time" selected="">Thời gian</option>
                    @else
                        <option value="time">Thời gian</option>
                    @endif

                    @if($sort == 'view')
                        <option value="view" selected="">Lượt xem</option>
                    @else
                        <option value="view">Lượt xem</option>
                    @endif

                    @if($sort == 'like')
                        <option value="like" selected="">Lượt thích</option>
                    @else
                        <option value="like">Lượt thích</option>
                    @endif
                    
                </select>
            </div>
            @if(!empty($categories))
            <div class="pull-right">
                Chủ đề
                <select name="category" class="form-control" style="display: inline-block; width: auto;">
                    @foreach ($categories as $category)
                        @if($categoryR == $category->category_title_clean)
                            <option value="{{$category->category_title_clean}}" selected>{{$category->category_title}}</option>
                        @else
                            <option value="{{$category->category_title_clean}}">{{$category->category_title}}</option>
                        @endif
                    @endforeach
                </select>
            </div>
            @endif
        </div>
        {!! Form::close() !!}

    <div class="list-item row">
        @foreach ($posts as $post)
            <div class="col-xs-6 col-sm-4">
                @include('pages.posts.partials.list-item')
            </div>
        @endforeach
    </div>
    <div class="clearfix">
        <div class="pull-right">
            {{ $posts->links() }}
        </div>
    </div>
    @else
        <p>Không có bài viết nào.</p>
    @endif
    <div id="container">

    </div>
@endsection

@section('inject_script')
    <script>
        $('#list-filter select').on('change', function () {
            $('#list-filter').submit();
        })
    </script>
@endsection
