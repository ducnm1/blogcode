<?php
/**
 * Created by PhpStorm.
 * User: duc91bk
 * Date: 25-Oct-16
 * Time: 11:17 PM
 */
?>
@extends('layouts.master-frontend')
@section('body_class', 'home-page')
@section('main')
    <div class="list-item row">
        @foreach ($items as $item)
            <div class="col-xs-6 col-sm-4">
                <div class="item">
                    <a href="{{ route('postDetail', ['categoryTitleClean'=> $item->category_title_clean, 'titleClean'=>$item->post_title_clean, 'id'=>$item->id]) }}">
                        <img src="{{ $item->post_thumbnail }}" alt="{{ $item->post_title }}">
                    </a>
                    <h3 class="item-title"><a href="{{ route('postDetail', ['categoryTitleClean'=> $item->category_title_clean, 'titleClean'=>$item->post_title_clean, 'id'=>$item->id]) }}">{{ $item->post_title }}</a></h3>
                    <p class="item-description">{{ $item->post_content  }}</p>
                    <a href="{{ route('postDetail', ['categoryTitleClean'=> $item->category_title_clean, 'titleClean'=>$item->post_title_clean, 'id'=>$item->id]) }}" class="btn btn-success">Xem chi tiết</a>
                </div>
            </div>
        @endforeach
    </div>
    <div class="clearfix">
        <div class="pull-right">
            {{ $items->links() }}
        </div>
    </div>
@endsection