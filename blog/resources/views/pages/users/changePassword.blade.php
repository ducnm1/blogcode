<?php
/**
 * Created by PhpStorm.
 * User: ducnm
 * Date: 1/11/2017
 * Time: 2:26 PM
 */
?>
@extends('layouts.master-backend')

@section('main')
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if(!empty($errorPassword))
        <div class="alert alert-danger">
            {{ $errorPassword }}
        </div>
    @endif

    {!! Form::open([
        'route' => ['updatePassword'],
        'method' => 'PUT'
    ]) !!}
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                {!! Form::label('oldPassword', 'Password cũ') !!}
                {!! Form::text('oldPassword',null, ['class'=>'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('newPassword', 'Password mới') !!}
                {!! Form::text('newPassword',null, ['class'=>'form-control']) !!}
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-success">Change</button>
    {!! Form::close() !!}
@endsection