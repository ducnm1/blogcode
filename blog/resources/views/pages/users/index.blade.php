<?php
/**
 * Created by PhpStorm.
 * User: duc91bk
 * Date: 16-Apr-17
 * Time: 12:32 AM
 */
?>
@extends('layouts.master-frontend')
@section('body_class', 'blog-page')
@section('main')
    <div class="row">
        <div class="col-md-3">
            <div class="user-profile">
                <h3>{{ $user->name }}</h3>
                <img src="{{ $user->avatar }}" alt="" class="img-responsive">
                @if(Auth::check() && Auth::id() != $user->id && !$statusFollow)
                    {!! Form::open([
                        'route' => 'followUser',
                        'method' => 'POST',
                    ]) !!}
                    <input type="hidden" name="user_id" value="{{ $user->id }}">
                    <button type="submit" class="btn btn-success">Follow</button>
                    {!! Form::close() !!}
                @elseif(Auth::check() && $statusFollow)
                    {!! Form::open([
                        'route' => 'unFollowUser',
                        'method' => 'DELETE',
                    ]) !!}
                    <input type="hidden" name="user_followed_id" value="{{ $user->id }}">
                    <button type="submit" class="btn btn-success">Unfollow</button>
                    {!! Form::close() !!}
                @endif
            </div>
            <div class="user-cv">
                <h3>Cvs</h3>
                <ul class="list-unstyled">
                    @foreach($cvs as $cv)
                    <li>
                        <p>{{ $cv->cv_career_title }}</p>
                        <p>{{ $cv->cv_title }}</p>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="col-md-9">
            <div class="list-item row">
                @foreach ($posts as $post)
                    <div class="col-xs-6 col-sm-4">
                        <div class="item">
                            <a href="{{ route('postDetail', ['titleClean'=>$post->post_title_clean, 'id'=>$post->id]) }}">
                                <img src="{{ $post->post_thumbnail }}" alt="{{ $post->post_title }}">
                            </a>
                            <h3 class="item-title"><a href="{{ route('postDetail', ['titleClean'=>$post->post_title_clean, 'id'=>$post->id]) }}">{{ $post->post_title }}</a></h3>
                            {{--<p class="item-description">{{ $post->post_content  }}</p>--}}
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="clearfix">
                <div class="pull-right">
                    {{ $posts->links() }}
                </div>
            </div>
        </div>
    </div>

@endsection
