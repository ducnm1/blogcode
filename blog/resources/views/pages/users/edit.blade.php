<?php
/**
 * Created by PhpStorm.
 * User: duc91bk
 * Date: 14-Dec-16
 * Time: 3:36 AM
 */
?>
@extends('layouts.master-backend')

@section('main')
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    {!! Form::model($user, [
        'route' => ['userUpdate', $user->id],
        'method' => 'PUT',
        'files'=>true
    ]) !!}

    <div class="form-group">
        {!! Form::label('name', 'Tên') !!}
        {!! Form::text('name',null, ['class'=>'form-control', 'readonly'=>'true']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('status', 'Trạng thái') !!}
        {!! Form::checkbox('status', $user->status) !!}
    </div>
    <div class="form-group">
        {!! Form::label('group_id', 'Nhóm') !!}
        {!! Form::select('group_id', $groupsTitle, null, ['class'=>'form-control']) !!}
    </div>

    <button type="submit" class="btn btn-success">Save</button>
    {!! Form::close() !!}
@endsection