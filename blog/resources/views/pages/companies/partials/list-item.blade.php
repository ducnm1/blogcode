<?php
/**
 * Created by PhpStorm.
 * User: duc91bk
 * Date: 16-Apr-17
 * Time: 10:45 AM
 */
?>
<div class="item">
    <a href="{{ route('companyDetail', ['titleClean'=>$company->company_title_clean, 'id'=>$company->id]) }}">
        <img src="{{ $company->company_thumbnail }}" alt="{{ $company->company_title }}">
    </a>
    <h3 class="item-title"><a href="{{ route('companyDetail', ['titleClean'=>$company->company_title_clean, 'id'=>$company->id]) }}">{{ $company->company_title }}</a></h3>
    <p class="item-description">{{ $company->company_address  }}</p>
    <p class="item-description"><a href="{{ $company->company_website  }}">{{ $company->company_website  }}</a></p>
    <p class="item-description">{{ $company->company_email  }}</p>
    <p class="item-description">{{ $company->company_size  }}</p>
    <p class="item-description">{{ $company->current_jobs  }} jobs</p>
</div>