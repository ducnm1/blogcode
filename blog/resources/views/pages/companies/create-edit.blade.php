<?php
/**
 * Created by PhpStorm.
 * User: ducnm
 * Date: 10/12/2016
 * Time: 10:43 AM
 */
?>

@extends('layouts.master-backend')

@section('main')
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if($currentRouteName == 'companyCreat')
            {!! Form::open([
                'route' => 'companyStore',
                'method' => 'POST',
                'files'=>true
            ]) !!}
        @else
            {!! Form::model($company, [
                'route' => ['companyUpdate', $company->id],
                'method' => 'PUT',
                'files'=>true
            ]) !!}
        @endif
            <div class="form-group">
                {!! Form::label('company_title', 'Tiêu đề') !!}
                {!! Form::text('company_title',null, ['class'=>'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('company_overview', 'Tổng quan') !!}
                {!! Form::textarea('company_overview',null, ['class'=>'form-control content-tinymce']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('company_address', 'Địa chỉ') !!}
                {!! Form::text('company_address', null, ['class'=>'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('company_website', 'Website') !!}
                {!! Form::text('company_website', null, ['class'=>'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('company_email', 'Email ') !!}
                {!! Form::text('company_email', null, ['class'=>'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('company_size', 'Quy mô công ty') !!}
                {!! Form::text('company_size', null, ['class'=>'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('company_thumbnail', 'Ảnh đại diện') !!}
                <div class="wrap-thumb">
                    {!! Form::file('company_thumbnail') !!}
                    <div class="image-holder">
                        @if(!empty($company->company_thumbnail))
                            <img src="{{ $company->company_thumbnail }}" alt="company_thumbnail">
                        @endif
                    </div>
                </div>
            </div>
            {{--<div class="row form-group">--}}
                {{--<div class="col-sm-4">--}}
                    {{--<div class="upload-thumbnail">--}}
                        {{--<div class="input-group">--}}
                            {{--@if(empty($company->company_thumbnail))--}}
                                {{--<input type="text" id="company_thumbnail" name="company_thumbnail" class="form-control" placeholder="Ảnh đại diện">--}}
                            {{--@else--}}
                                {{--<input type="text" id="company_thumbnail" name="company_thumbnail" class="form-control" placeholder="Ảnh đại diện" value="{{ $company->company_thumbnail  }}">--}}
                            {{--@endif--}}
                            {{--<span class="input-group-btn">--}}
                                {{--<a data-toggle="modal" class="btn btn-default" href="javascript:;" data-target="#upload-file-manager">Select</a>--}}
                            {{--</span>--}}
                        {{--</div><!-- /input-group -->--}}
                        {{--<div class="upload-thumbnail-preview-image hide">--}}

                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}


            <button type="submit" class="btn btn-success">Save</button>
        {!! Form::close() !!}
    <div class="modal fade" id="upload-file-manager" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Upload ảnh đại diện</h4>
                </div>
                <div class="modal-body">
                    <iframe width="100%" height="600px"
                            src="/filemanager/dialog.php?type=2&amp;field_id=company_thumbnail'&amp;fldr=" frameborder="0"
                            style="overflow: scroll; overflow-x: hidden; overflow-y: scroll; "></iframe>
                </div>

            </div>
        </div>
    </div>
    <style>
        @media (min-width: 768px) {
            #upload-file-manager .modal-dialog {
                width: 1200px;
            }

            #upload-file-manager .modal-content {
                overflow: hidden;
            }

            #upload-file-manager .modal-body {
                padding: 0;
                background: #ececec;
            }
        }
        .upload-thumbnail-preview-image {
            margin-top: 20px;
        }
        .upload-thumbnail-preview-image img {
            width: 100%;
            height: auto;
            display: block;
        }
    </style>
    <script>
        previewThumbnail();
        $('#company_thumbnail').change(function () {
            previewThumbnail();
        });
        $('#upload-file-manager').on('hidden.bs.modal', function (e) {
            previewThumbnail();
        })

        function previewThumbnail() {
            if ($('#company_thumbnail').val())
            {
                var $companyThumbnail = $('#company_thumbnail').val();
                var $domain = window.location.origin;
                $companyThumbnail = $companyThumbnail.replace($domain, '');
                var $pathThumbnail = $companyThumbnail.split('/');
                if ($pathThumbnail[1] == 'upload')
                {
                    var $filename = $companyThumbnail.replace(/^.*[\\\/]/, '');
                    $companyThumbnail = "/thumbs/" + $filename;
                    $('#company_thumbnail').val($companyThumbnail);
                }
                var $img = '<img src="'+ $companyThumbnail +'" />';
                $('.upload-thumbnail-preview-image').removeClass('hide').html($img);
            }
        }

    </script>

    <script>
        upload_thumbnail ();
        function upload_thumbnail () {
            var myReader = new FileReader();
            $("input[type='file']").on('change', function () {
                var $this = $(this);
                if (typeof (FileReader) != "undefined") {
                    var image_holder = $this.closest('.wrap-thumb').find(".image-holder");
                    image_holder.empty();
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $("<img />", {
                            "src": e.target.result,
                            "class": "thumb-image"
                        }).appendTo(image_holder);
                    }
                    reader.readAsDataURL($(this)[0].files[0]);
                } else {
                    alert("This browser does not support FileReader.");
                }
            });
        }
    </script>
    <style>
        .image-holder {
            margin-top: 10px;
            max-width: 400px;
        }
        .image-holder img {
            display: block;
            max-width: 100%;
            height: auto;
        }
    </style>
@endsection