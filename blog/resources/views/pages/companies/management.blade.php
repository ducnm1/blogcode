<?php
/**
 * Created by PhpStorm.
 * User: duc91bk
 * Date: 25-Oct-16
 * Time: 11:17 PM
 */
?>
@extends('layouts.master-backend')

@section('main')
    @if(count($companies))
    <table class="table table-responsive table-striped table-bordered">
        <thead>
            <tr>
                <th>#</th>
                <th>Công ty</th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        @foreach ($companies as $company)
            <tr>
                <td>{{ $loop->index }}</td>
                <td><a href="{{ route('companyDetail', ['titleClean'=>$company->company_title_clean, 'id'=>$company->id]) }}">{{ $company->company_title }}</a></td>


                <td><a href="{{ route('companyEdit', ['id' => $company->id]) }}">Sửa</a></td>

                <td><a href="{{ route('companyDestroy', ['id' => $company->id]) }}">Xóa</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @else
        <p>Bạn chưa quản lý công ty nào.
            <a href="{{ route('companyCreat') }}" class="btn btn-primary">Tạo công ty</a>
        </p>
    @endif
    {{ $companies->links() }}
@endsection
