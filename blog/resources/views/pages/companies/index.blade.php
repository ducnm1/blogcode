<?php
/**
 * Created by PhpStorm.
 * User: duc91bk
 * Date: 16-Apr-17
 * Time: 3:02 AM
 */
?>
@extends('layouts.master-frontend')
@section('body_class', 'blog-page')
@section('main')
    <div class="row">
        <div class="col-md-3">
            <div class="user-profile">
                <h3>{{ $company->company_title }}</h3>
                <img src="{{ $company->company_thumbnail }}" alt="" class="img-responsive">
                @if(Auth::check() && !$statusFollow)
                    {!! Form::open([
                        'route' => 'followCompany',
                        'method' => 'POST',
                    ]) !!}
                    <input type="hidden" name="company_id" value="{{ $company->id }}">
                    <button type="submit" class="btn btn-success">Follow</button>
                    {!! Form::close() !!}
                @elseif(Auth::check() && $statusFollow)
                    {!! Form::open([
                        'route' => 'unFollowCompany',
                        'method' => 'DELETE',
                    ]) !!}
                    <input type="hidden" name="company_id" value="{{ $company->id }}">
                    <button type="submit" class="btn btn-success">Unfollow</button>
                    {!! Form::close() !!}
                @endif
            </div>
        </div>
        <div class="col-md-9">
            <div class="list-item row">
                @foreach ($jobs as $job)
                    <div class="col-xs-6 col-sm-4">
                        @include('pages.jobs.partials.list-item')
                    </div>
                @endforeach
            </div>
            <div class="clearfix">
                <div class="pull-right">
                    {{ $jobs->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection
