<?php
/**
 * Created by PhpStorm.
 * User: duc91bk
 * Date: 16-Apr-17
 * Time: 3:15 AM
 */
?>
<div class="item">
    <a href="{{ route('jobDetail', ['titleClean'=>$job->job_title_clean, 'id'=>$job->id]) }}">
        <img src="{{ $job->company_thumbnail }}" alt="{{ $job->job_title }}">
    </a>
    <h3 class="item-title"><a href="{{ route('jobDetail', ['titleClean'=>$job->job_title_clean, 'id'=>$job->id]) }}">{{ $job->job_title }}</a></h3>
    <p class="item-description">{{ $job->job_address  }}</p>
    <p class="item-description">{{ $job->job_sallary  }}</p>
    <p class="item-description">{{ $job->job_skill  }}</p>
    <p class="item-description">{{ $job->job_expired  }}</p>
</div>
