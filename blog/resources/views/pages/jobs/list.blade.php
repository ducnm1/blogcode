<?php
/**
 * Created by PhpStorm.
 * User: duc91bk
 * Date: 13-Apr-17
 * Time: 3:50 PM
 */
?>
@extends('layouts.master-frontend')
@section('body_class', 'home-page')
@section('main')
    <div class="list-item">
        <div class="row">
            @foreach ($jobs as $job)
                <div class="col-md-4">
                    @include('pages.jobs.partials.list-item')
                </div>
            @endforeach
        </div>
    </div>
    <div class="clearfix">
        <div class="pull-right">
            {{ $jobs->links() }}
        </div>
    </div>
@endsection
