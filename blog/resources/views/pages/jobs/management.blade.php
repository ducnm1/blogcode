<?php
/**
 * Created by PhpStorm.
 * User: duc91bk
 * Date: 25-Oct-16
 * Time: 11:17 PM
 */
?>
@extends('layouts.master-backend')

@section('main')
    @if(count($jobs))
    <table class="table table-responsive table-striped table-bordered">
        <thead>
            <tr>
                <th>#</th>
                <th>Tiêu đề</th>
                <th>Công ty</th>
                <th>Nghề nghiệp</th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        @foreach ($jobs as $job)
            <tr>
                <td>{{ $loop->index }}</td>
                <td><a href="{{ route('jobDetail', ['titleClean'=> $job->job_title_clean, 'id'=>$job->id]) }}">{{ $job->job_title }}</a></td>
                <td>{{ $job->company_title }}</td>
                <td>{{ $job->career_title }}</td>
                <td><a href="{{ route('jobEdit', ['id' => $job->id]) }}">Sửa</a></td>
                <td><a href="{{ route('jobDestroy', ['id' => $job->id]) }}">Xóa</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @else
        <p>Bạn chưa có job nào.

            <a href="{{ route('jobCreat') }}" class="btn btn-primary">Tạo job</a>
        </p>
    @endif
    {{ $jobs->links() }}
@endsection
