<?php
/**
 * Created by PhpStorm.
 * User: ducnm
 * Date: 10/6/2016
 * Time: 4:07 PM
 */
?>
@extends('layouts.master-frontend')

@section('main')
    <div class="row">
        <aside id="sidebar-left" class="col-xs-12 col-md-3">
            <div class="widget widget-company">
                <img src="{{ $company->company_thumbnail }}" alt="{{ $company->company_title }}">
                <ul class="list-unstyled">
                    <li><a href="{{ $company->company_website }}">{{ $company->company_website }}</a></li>
                    <li>{{ $company->company_size }}</li>
                    <li>{{ $company->company_address }}</li>
                    <li>{{ $company->currentJobs }} jobs</li>
                </ul>
            </div>
        </aside>
        <!-- sidebar left -->
        <section id="main-col" class="col-xs-12 col-sm-12 col-md-9">
            <div class="entry-post">
                <h1 class="entry-title">{{ $item->job_title }}</h1>
                <p>{{ $item->job_address }}</p>
                <p>{{ $item->job_salary }}</p>

                @if(Auth::check())
                    {!! Form::open([
                        'route' => 'applyCv',
                        'method' => 'POST',
                    ]) !!}
                    <input type="hidden" class="form-control" name="company_id" value="{{ $company->id }}">
                    <div class="input-group">
                        {!! Form::select('cv_id', $cvs, null, ['class'=>'form-control']) !!}
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-success ">Apply cv</button>
                        </div>
                    </div>

                    {!! Form::close() !!}
                @endif

                @if(!empty($item->job_skill))
                <p>{{ $item->job_skill }}</p>
                @endif

                @if(!empty($item->job_content))
                <div class="job-content">
                    <h3>The job</h3>
                    <div class="content">
                        {{ $item->job_content }}
                    </div>
                </div>
                @endif

                @if(!empty($item->job_experience))
                <div class="job-experience">
                    <h3>Your Skills and Experience</h3>
                    <div class="content">
                        {{ $item->job_experience }}
                    </div>
                </div>
                @endif

                @if(!empty($item->job_benefit))
                <div class="job-benefit">
                    <h3>The benefit</h3>
                    <div class="content">
                        {{ $item->job_benefit }}
                    </div>
                </div>
                @endif
            </div>
            {{-- entry post --}}

            <section class="job-related">
                <h3>Các công việc tương tự</h3>
                <ul>
                    @foreach ($relatedJobs as $relatedJob)
                        <li class="item">
                            <a href="{{ route('jobDetail', ['titleClean'=> $relatedJob->job_title_clean, 'id'=>$relatedJob->id]) }}">
                                <img src="{{ $relatedJob->company_thumbnail }}" alt="{{ $relatedJob->job_title }}">
                            </a>
                            <h3 class="item-title"><a href="{{ route('jobDetail', ['titleClean'=> $relatedJob->job_title_clean, 'id'=>$relatedJob->id]) }}">{{ $relatedJob->job_title }}</a></h3>
                            <p class="item-description">{{ $relatedJob->job_content  }}</p>
                        </li>
                    @endforeach
                </ul>
            </section>
        </section>
        <!-- main col -->
    </div>
@endsection
