/**
 * plugin.js
 *
 * Released under LGPL License.
 * Copyright (c) 1999-2015 Ephox Corp. All rights reserved
 *
 * License: http://www.tinymce.com/license
 * Contributing: http://www.tinymce.com/contributing
 */

/*jshint unused:false */
/*global tinymce:true */

/**
 * Example plugin that adds a toolbar button and menu item.
 */
tinymce.PluginManager.add('codehightlight', function(editor, url) {
	var languages = [
		{text: 'Css', value: 'css'},
		{text: 'Javascript', value: 'javascript'},
		{text: 'Html', value: 'html'},
		{text: 'Php', value: 'php'}
	];
	function buildListItems(inputList, itemCallback, startItems) {
		function appendItems(values, output) {
			output = output || [];

			tinymce.each(values, function(item) {
				var menuItem = {text: item.text || item.title};

				if (item.menu) {
					menuItem.menu = appendItems(item.menu);
				} else {
					menuItem.value = item.value;

					if (itemCallback) {
						itemCallback(menuItem);
					}
				}

				output.push(menuItem);
			});

			return output;
		}

		return appendItems(inputList, startItems || []);
	}
	// Add a button that opens a window
	editor.addButton('codehightlight', {
		text: 'Code hightlight',
		icon: false,
		onclick: function() {
			// Open window
			editor.windowManager.open({
				title: 'Code hightlight',
				width: 800,
				height: 550,
				body: [
					{
						type: 'listbox',
						name: 'language',
						label: 'Language',
						values: buildListItems(languages)
					},
					{
						type: 'textbox',
						name: 'title',
						multiline: true,
						minWidth: editor.getParam("code_dialog_width", 600),
						minHeight: editor.getParam("code_dialog_height", Math.min(tinymce.DOM.getViewPort().h - 200, 500)),
						label: 'Code'
					}

				],
				onsubmit: function(e) {
					// Insert content when the window form is submitted

					editor.insertContent('<pre style="border: 1px dashed #ccc; padding: 10px;"><code class="language-'+ e.data.language +'">' + e.data.title + '</code></pre>');
				}
			});
		}
	});
});