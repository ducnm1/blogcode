<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Auth::routes();

Route::get('/logout', 'Auth\LogoutController@logout')->name('logout');
Route::get('/crawler', 'PostController@crawlerData');
Route::get('/redirect', function () {
    $query = http_build_query([
        'client_id' => '5',
        'redirect_uri' => 'http://localhost:3000',
        'response_type' => 'code',
        'scope' => '',
    ]);

    return redirect('http://localhost:8000/oauth/authorize?'.$query);
});

Route::get('/callback', function (Illuminate\Http\Request $request) {
    $http = new \GuzzleHttp\Client;



    $response = $http->post('http://localhost:8000/oauth/token', [
        'form_params' => [
            'client_id' => '5',
            'client_secret' => 'b72aHGmiaTYYADws3Cyt9YnfBkhQXxuqrEb1Gt39',
            'grant_type' => 'authorization_code',
            'redirect_uri' => 'http://localhost:3000/callback',
            'code' => "TD3EKLJJ2LDaK6sInsyUgh2gH3BFpZFehBtEorDKxElcETBqLTDTTIEGYHqF/fblFtz2N22ha0eV6GGt9XK6uSFVzlAxKVx3o05WLZtiFnkgltLQSQPbFZ+7Fbghwrn2BvF95PLKW4+rbw3FlWli0ppIvGGQMBqH3Ady+xW9H/gBP7EiDvQaGoMXeIQig2u9APmfmL0HTiLsUQOheF5jd3HT7ovLY+mGV/tAG/ctbNwwe9vxD1tNpiZ2ZhVB0e3BjD6KB/kK2b7TmOA9ThUF8ic48wvf0Ln/YaMEuNy7CjeqHCZnQL0rl75PmxY5siXwnWUNljt/yYa8vJV+Z5PEFsDz6/sT3y+qQIAmBpJJZrF7P2U8MNWh1/A8vPF2wxU4597bOMaq9ghgEagb5kJEbBBAC/k+LoEiQGpgHUKYpbItKotVgMZrg+iSzD3tb89LZ0aBk3O0fjOzGvjB+f9S/PNiG03rJuw1HdPgWs6FYesIRSeSFfdSP7uGgGM62NFGIoEACm0oSEgVHf3lAZ/pYBguwEOkxFrWVc6WDN/xGAjfzlEw9btyo5LjgI8N7RLMGWCjPapWN2fMWuKpornRoCaO2uRZ+uP7GgsMObi1KZnQqg8vy4L3GdL3a1XDaFx+OIxZfQEXsrIjZvHtUsxjDa/nYi/vV5h8Y6BxrYSEjzw=",
        ],
    ]);
    return json_decode((string) $response->getBody(), true);
});
/*
 * User
 * */

Route::group(['middleware' => ['auth', 'backendmenu']], function () {
    Route::get('/admin', 'DashboardController@index')->name('dashboard');

    Route::get('/permission', 'UserController@permission')->name('permission');
    Route::put('/permission', 'UserController@permissionUpdate')->name('permissionUpdate');

    Route::get('/user/management', 'UserController@index')->name('usersManagement');
    Route::get('/user/changePassword', 'UserController@changePassword')->name('changePassword');
    Route::put('/user/changePassword', 'UserController@updatePassword')->name('updatePassword');
    Route::get('/user/{id}/edit', 'UserController@edit')->name('userEdit');
    Route::put('/user/{id}', 'UserController@update')->name('userUpdate');

    Route::post('/user/follow', 'FollowUserController@create')->name('followUser');
    Route::delete('/user/unfollow', 'FollowUserController@destroy')->name('unFollowUser');
});

Route::group(['middleware' => ['frontendmenu']], function () {
    Route::get('/user/{username}', 'UserController@index')->name('user');
});

/*
 * Setting
 * */

Route::group(['middleware' => ['auth', 'backendmenu']], function () {
    Route::get('/setting', 'OptionController@edit')->name('optionEdit');
    Route::put('/setting/{id}', 'OptionController@update')->name('optionUpdate');
});

/*
 * Category
 * */


Route::group(['middleware' => ['auth', 'backendmenu']], function () {
    Route::get('/category/create', 'CategoryController@create')->name('categoryCreat');
    Route::post('/category', 'CategoryController@store')->name('categoryStore');
    Route::get('/category/{id}/edit', 'CategoryController@edit')->name('categoryEdit');
    Route::put('/category/{id}', 'CategoryController@update')->name('categoryUpdate');
    Route::get('/category/{id}/destroy', 'CategoryController@destroy')->name('categoryDestroy');
    Route::get('/category/management', 'CategoryController@categoriesManagement')->name('categoriesManagement');
});

/*
 * Post
 * */

Route::group(['middleware' => 'frontendmenu'], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/home', function (){
        return redirect()->route('home');
    });

    //Route::get('/blog', 'PostController@index')->name('postList');
    Route::get('/blog/{titleClean}-{id}', 'PostController@show')->name('postDetail');

    //Route::get('/category/{categoryTitleClean}', 'PostController@postsOfCategory')->name('postsOfCategory');
    Route::get('/blog/{category?}{sort?}', 'PostController@postsOfCategory')->name('postsOfCategory');
    Route::get('/tag/{tag?}{sort?}', 'PostController@postsOfTag')->name('postsOfTag');
});

Route::group(['middleware' => ['auth', 'backendmenu']], function () {
    Route::get('/post/create', 'PostController@create')->name('postCreat');
    Route::post('/post', 'PostController@store')->name('postStore');
    Route::get('/post/{id}/edit', 'PostController@edit')->name('postEdit');
    Route::put('/post/{id}', 'PostController@update')->name('postUpdate');
    Route::get('/post/{id}/destroy', 'PostController@destroy')->name('postDestroy');
    Route::get('/post/management', 'PostController@management')->name('postsManagement');
});

/*
 * Tag
 * */
Route::group(['middleware' => 'frontendmenu'], function () {

});

/*
 * Job
 * */

Route::group(['middleware' => 'frontendmenu'], function () {
    Route::get('/job', 'JobController@index')->name('jobList');
    Route::get('/job/{titleClean}-{id}', 'JobController@show')->name('jobDetail');
});

Route::group(['middleware' => ['auth', 'backendmenu'], 'prefix' => 'admin'], function () {
    Route::get('/job', 'JobController@management')->name('jobsManagement');
    Route::get('/job/create', 'JobController@create')->name('jobCreat');
    Route::post('/job', 'JobController@store')->name('jobStore');
    Route::get('/job/{id}/edit', 'JobController@edit')->name('jobEdit');
    Route::put('/job/{id}', 'JobController@update')->name('jobUpdate');
    Route::get('/job/{id}/destroy', 'JobController@destroy')->name('jobDestroy');
});

/*
 * Company
 * */

Route::group(['middleware' => 'frontendmenu'], function () {
    Route::get('/company', 'CompanyController@index')->name('companyList');
    Route::get('/company/{titleClean}-{id}', 'CompanyController@show')->name('companyDetail');
});

Route::group(['middleware' => ['auth', 'backendmenu'], 'prefix' => 'admin'], function () {
    Route::get('/company', 'CompanyController@management')->name('companiesManagement');
    Route::get('/company/create', 'CompanyController@create')->name('companyCreat');
    Route::post('/company', 'CompanyController@store')->name('companyStore');
    Route::get('/company/{id}/edit', 'CompanyController@edit')->name('companyEdit');
    Route::put('/company/{id}', 'CompanyController@update')->name('companyUpdate');
    Route::get('/company/{id}/destroy', 'CompanyController@destroy')->name('companyDestroy');
    Route::post('/company/follow', 'FollowCompanyController@create')->name('followCompany');
    Route::delete('/company/unfollow', 'FollowCompanyController@destroy')->name('unFollowCompany');
});

/*
 * Cv
 * */

Route::group(['middleware' => 'frontendmenu'], function () {
    Route::get('/cv/{titleClean}-{id}', 'CvController@show')->name('cvDetail');

});
Route::group(['middleware' => ['auth']], function () {
    Route::post('/apply-cv', 'CvController@applyCv')->name('applyCv');
});

Route::group(['middleware' => ['auth', 'backendmenu'], 'prefix' => 'admin'], function () {
    Route::get('/cv', 'CvController@management')->name('cvsManagement');
    Route::get('/cv/create', 'CvController@create')->name('cvCreat');
    Route::post('/cv', 'CvController@store')->name('cvStore');
    Route::get('/cv/{id}/edit', 'CvController@edit')->name('cvEdit');
    Route::put('/cv/{id}', 'CvController@update')->name('cvUpdate');
    Route::get('/cv/{id}/destroy', 'CvController@destroy')->name('cvDestroy');
});

/*
 * Blog detail
 * */
Route::group(['middleware' => 'frontendmenu'], function () {
    Route::get('/search/{searchIn?}{searchKey?}', 'SearchController@search')->name('search');

});


Route::get('/checkAuth', 'UserController@checkAuth');



