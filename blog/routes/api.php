<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

//Route::get('/blogs', 'PostController@apiIndex');
Route::get('/blog/{id}', 'PostController@apiShow');
Route::put('/blog/post/like/{idPost}/{idUser}', 'PostLikeController@apiLike');
Route::get('/post-like/{idPost}/{idUser}', 'PostLikeController@apiCheckStatusLiked');
Route::put('/blog/post/view/{id}', 'PostController@apiView');

Route::get('/blog/categories', 'CategoryController@apiGetList');